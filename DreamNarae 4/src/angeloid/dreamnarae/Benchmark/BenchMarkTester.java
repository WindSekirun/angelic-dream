/*
 * Copyright (C) 2010 0xlab - http://0xlab.org/
 *
 * Licensed under the Apace License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package angeloid.dreamnarae.Benchmark;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.view.Window;
import android.widget.ImageView;
import angeloid.Narae.NaraePreference;
import angeloid.Narae.NaraeTools;
import angeloid.dreamnarae.Shared.StackManager;
import angeloid.dreamnarae.v4.CompatibleActivity;
import angeloid.dreamnarae.v4.R;

@SuppressLint("HandlerLeak")
public class BenchMarkTester extends Tester {
	public static double time = 0.0;
	public static Handler mHandler, imgHandler;
	public static int result;
	public NaraePreference np;
	public ImageView iv;
	public StackManager am = StackManager.getInstance();
	public NaraeTools nt = NaraeTools.getInstance(this);

	public String getTag() {
		return "GC";
	}

	public int sleepBeforeStart() {
		return 1000;
	}

	public int sleepBetweenRound() {
		return 0;
	}

	public void oneRound() {
		GCBenchmark.benchmark();
		decreaseCounter();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		GCBenchmark.check = 0;
		requestWindowFeature(Window.FEATURE_PROGRESS);
		am.addActivity(this);
		setContentView(R.layout.gc);
		ActionBar ab = getSupportActionBar();
		ab.setHomeButtonEnabled(true);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowHomeEnabled(true);
		ab.setDisplayShowTitleEnabled(true);
		ab.setTitle(R.string.activity_compatible);
		ab.setIcon(R.drawable.ic_actionbar);
		setProgressBarIndeterminateVisibility(true);
		setSupportProgressBarIndeterminateVisibility(true);
		iv = (ImageView) findViewById(R.id.imageView1);
		np = new NaraePreference(BenchMarkTester.this);
		mHandler = new Handler() {
			public void handleMessage(Message msg) {
				np.put("result", msg.what);
				np.put("last_benchmark", nt.getTime());
				AlertDialog.Builder d = new AlertDialog.Builder(BenchMarkTester.this);
				d.setMessage(getResources().getString(R.string.alert_maketree));
				d.setPositiveButton(android.R.string.yes, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Intent i = new Intent(BenchMarkTester.this, CompatibleActivity.class);
						startActivity(i);
						overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
					}
				});
				d.setNegativeButton(android.R.string.no, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						android.os.Process.killProcess(android.os.Process.myPid());
					}
				});
				d.show();
				super.handleMessage(msg);
			}
		};
		imgHandler = new Handler() {
			public void handleMessage(Message msg) {
				int value = msg.what;
				if (value == 4) {
					iv.setImageDrawable(getResources().getDrawable(R.drawable.benchmark2));
				} else if (value == 8) {
					iv.setImageDrawable(getResources().getDrawable(R.drawable.benchmark3));
				} else if (value == 11) {
					iv.setImageDrawable(getResources().getDrawable(R.drawable.benchmark4));
				}
				super.handleMessage(msg);
			}
		};
	}

	@Override
	public void onBackPressed() {
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		am.removeActivity(this);
	}

	@Override
	public void onResume() {
		super.onResume();
		startTester();
	}

	public static class GCBenchmark {
		public static final int kStretchTreeDepth = 16;
		public static final int kLongLivedTreeDepth = 14;
		public static final int kArraySize = 125000;
		public static final int kMinTreeDepth = 2;
		public static final int kMaxTreeDepth = 8;
		public static Node root;
		public static Node longLivedTree;
		public static Node tempTree;
		public static long tStart, tFinish;
		public static long tElapsed;
		public static long lTotalMemory, lFreeMemory;
		public static int check = 0;
		public static int value;
		public static Context c;
		public static StringBuffer out = new StringBuffer();

		public GCBenchmark(Context mc) {
			c = mc;
		}

		static void update(String s) {
			check++;
			value = value + Integer.valueOf(s);
			if (check == 4) {
				BenchMarkTester.imgHandler.sendEmptyMessage(4);
			} else if (check == 8) {
				BenchMarkTester.imgHandler.sendEmptyMessage(8);
			} else if (check == 11) {
				BenchMarkTester.imgHandler.sendEmptyMessage(11);
			} else if (check == 12) {
				Message m = new Message();
				m.what = value;
				BenchMarkTester.mHandler.sendMessage(m);
			}
		}

		static int TreeSize(int i) {
			return ((1 << (i + 1)) - 1);
		}

		static int NumIters(int i) {
			return 2 * TreeSize(kStretchTreeDepth) / TreeSize(i);
		}

		static void Populate(int iDepth, Node thisNode) {
			if (iDepth <= 0) {
				return;
			} else {
				iDepth--;
				thisNode.left = new Node();
				thisNode.right = new Node();
				Populate(iDepth, thisNode.left);
				Populate(iDepth, thisNode.right);
			}
		}

		static Node MakeTree(int iDepth) {
			if (iDepth <= 0) {
				return new Node();
			} else {
				return new Node(MakeTree(iDepth - 1), MakeTree(iDepth - 1));
			}
		}

		static void PrintDiagnostics() {
			lFreeMemory = Runtime.getRuntime().freeMemory();
			lTotalMemory = Runtime.getRuntime().totalMemory();
		}

		static void TimeConstruction(int depth) {
			int iNumIters = NumIters(depth);
			Node tempTree;
			tStart = System.currentTimeMillis();
			for (int i = 0; i < iNumIters; ++i) {
				tempTree = new Node();
				Populate(depth, tempTree);
				tempTree = null;
			}
			tFinish = System.currentTimeMillis();
			tStart = System.currentTimeMillis();
			for (int i = 0; i < iNumIters; ++i) {
				tempTree = MakeTree(depth);
				tempTree = null;
			}
			tFinish = System.currentTimeMillis();
		}

		public static void benchmark() {
			out = new StringBuffer();
			PrintDiagnostics();
			tStart = System.currentTimeMillis();
			tempTree = MakeTree(kStretchTreeDepth);
			tempTree = null;
			longLivedTree = new Node();
			Populate(kLongLivedTreeDepth, longLivedTree);
			double array[] = new double[kArraySize];
			for (int i = 0; i < kArraySize / 2; ++i) {
				array[i] = 1.0 / i;
			}
			PrintDiagnostics();
			for (int d = kMinTreeDepth; d <= kMaxTreeDepth; d += 2) {
				TimeConstruction(d);
			}
			if (longLivedTree == null || array[1000] != 1.0 / 1000) update("Failed");
			tFinish = System.currentTimeMillis();
			tElapsed = tFinish - tStart;
			PrintDiagnostics();
			update(String.valueOf(tElapsed));
			BenchMarkTester.time = tElapsed;
		}
	}
}
