/*
 * Copyright (C) 2010 0xlab - http://0xlab.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package angeloid.dreamnarae.Benchmark;

import java.util.ArrayList;
import android.content.Intent;
import android.util.Log;

public abstract class Case {
	public String TAG = "Case", TESTER, mType = "";
	public int mRepeatMax = 1, mRepeatNow;
	public boolean mInvolved;
	public long[] mResult;
	public final static String SOURCE = "SOURCE", INDEX = "INDEX", RESULT = "RESULT", ROUND = "ROUND";
	public int mCaseRound = 30;
	public String[] mTags = {};

	protected Case(String tag, String tester, int repeat, int round) {
		TAG = tag;
		TESTER = tester;
		mRepeatMax = repeat;
		mCaseRound = round;
		reset();
	}

	abstract public String getDescription();

	abstract public String getTitle();

	abstract public ArrayList<Scenario> getScenarios();

	public final static void putRound(Intent intent, int round) {
		intent.putExtra(ROUND, round);
	}

	public final static void putIndex(Intent intent, int index) {
		intent.putExtra(INDEX, index);
	}

	public final static void putSource(Intent intent, String source) {
		intent.putExtra(SOURCE, source);
	}

	public final static void putResult(Intent intent, long result) {
		intent.putExtra(RESULT, result);
	}

	public final static int getRound(Intent intent) {
		return intent.getIntExtra(ROUND, 100);
	}

	public final static int getIndex(Intent intent) {
		return intent.getIntExtra(INDEX, -1);
	}

	public final static String getSource(Intent intent) {
		String source = intent.getStringExtra(SOURCE);
		if (source == null) {
			return "unknown";
		}
		if (source.equals("")) {
			return "unknown";
		}
		return source;
	}

	public final static long getResult(Intent intent) {
		long defaultResult = -1;
		return intent.getLongExtra(RESULT, defaultResult);
	}

	public String getTag() {
		return TAG;
	}

	public void clear() {
		mResult = new long[mRepeatMax];
		mRepeatNow = mRepeatMax;
		mInvolved = false;
	}

	public void reset() {
		mResult = new long[mRepeatMax];
		mRepeatNow = 0;
		mInvolved = true;
	}

	public boolean isFinish() {
		return (mRepeatNow >= mRepeatMax);
	}

	public boolean realize(Intent intent) {
		if (intent == null) {
			Log.i(TAG, "Intent is null");
			return false;
		}
		String source = Case.getSource(intent);
		if (source == null || source.equals("")) {
			return false;
		}
		if (source.equals(TAG)) {
			return true;
		} else {
			return false;
		}
	}

	public double getBenchmark(Scenario s) {
		double total = 0;
		int length = mResult.length;
		for (int i = 0; i < length; i++) {
			total += mResult[i];
		}
		return (double) total / length;
	}
}
