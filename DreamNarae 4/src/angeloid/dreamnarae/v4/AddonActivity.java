package angeloid.dreamnarae.v4;

import java.util.ArrayList;
import java.util.Arrays;
import org.holoeverywhere.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import angeloid.LilyAngels.Tweak.Addon.RunFstrim;
import angeloid.Narae.NaraePreference;
import angeloid.Narae.NaraeTools;
import angeloid.dreamnarae.Base.BaseListViewActivity;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class AddonActivity extends BaseListViewActivity {
	
	public ActionBar.TabListener tabListener;
	public ListView list;
	public static ArrayList<String> addon_title;
	public static ArrayList<String> addon_subtitle;
	public NaraePreference np;
	public ProgressDialog mDlg;
	public NaraeTools nf = NaraeTools.getInstance(this);

	@Override
	public void onCreate(Bundle s) {
		super.onCreate(s);
		am.addActivity(this);
		setContentView(R.layout.menulist);
		init();
		np = new NaraePreference(AddonActivity.this);
		list = (ListView) findViewById(R.id.listView1);
		View header = getLayoutInflater().inflate(R.layout.addon, null);
		list.addHeaderView(header);		
		new LoadData().execute();
		ab.addTab(Economy.setTabListener(new FakeListener()));
		ab.addTab(Tweak.setTabListener(new FakeListener()));
		ab.setSelectedNavigationItem(1);
		Economy.setIcon(R.drawable.ic_economy_disable);
		Tweak.setIcon(R.drawable.ic_tweak_enable);
		Economy.setTabListener(new TabListener());
		Tweak.setTabListener(new TabListener());
	}

	@Override
	public void onBackPressed() {
		Crouton.cancelAllCroutons();
		Intent i = new Intent(this, MainActivity.class);
		i.putExtra("value", 1);
		startActivity(i);
	}

	public class FakeListener implements ActionBar.TabListener {
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}

	public class TabListener implements ActionBar.TabListener {
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			int i = tab.getPosition();
			if (i == 0) {
				Economy.setIcon(R.drawable.ic_economy_enable);
				Tweak.setIcon(R.drawable.ic_tweak_disable);
				Intent a = new Intent(AddonActivity.this, MainActivity.class);
				a.putExtra("value", 0);
				startActivity(a);
			} else if (i == 1) {}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}

	public class ClickMenu implements AdapterView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			if (position == 5) {
				new AllRun().execute();
			} else if (position == 1) {
				Intent i = new Intent(AddonActivity.this, EntropyActivity.class);
				startActivity(i);
				Crouton.cancelAllCroutons();
			} else if (position == 2) {
				Intent i = new Intent(AddonActivity.this, TrimActivity.class);
				startActivity(i);
				Crouton.cancelAllCroutons();
			} else if (position == 3) {
				Intent i = new Intent(AddonActivity.this, ZipAlignActivity.class);
				startActivity(i);
				Crouton.cancelAllCroutons();
			} else if (position == 4) {
				Intent i = new Intent(AddonActivity.this, SQLite3Activity.class);
				startActivity(i);
				Crouton.cancelAllCroutons();
			}
		}
	}

	public class AllRun extends AsyncTask<Void, Void, Void> {
		@Override
		public void onPostExecute(Void result) {
			mDlg.dismiss();
			Crouton.makeText(AddonActivity.this, getResources().getString(R.string.crouton_done), Style.INFO).show();
			super.onPostExecute(result);
		}

		@Override
		public void onPreExecute() {
			mDlg = new ProgressDialog(AddonActivity.this);
			mDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mDlg.setIndeterminate(false);
			mDlg.setCancelable(true);
			mDlg.setMessage(getResources().getString(R.string.progress_work));
			mDlg.setCanceledOnTouchOutside(false);
			mDlg.show();
			super.onPreExecute();
		}

		@Override
		public Void doInBackground(Void... params) {
			np.put("service_last_entropy", nf.getTime());
			new RunFstrim(AddonActivity.this).execute();
			np.put("service_last_trim", nf.getTime());
			np.put("service_last_zipalign", nf.getTime());
			np.put("service_last_sqlite3", nf.getTime());
			return null;
		}
	}

	public class LoadData extends AsyncTask<Void, Void, Void> {
		@Override
		public void onPostExecute(Void result) {
			list.setAdapter(adapter);
			list.setOnItemClickListener(new ClickMenu());
			super.onPostExecute(result);
		}

		@Override
		public Void doInBackground(Void... arg0) {
			addon_title = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.addon_menu_title)));
			addon_subtitle = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.addon_menu_subtitle)));
			ArrayList<Drawable> icon = new ArrayList<Drawable>();
			ArrayList<Drawable> background = new ArrayList<Drawable>();
			icon.add(getResources().getDrawable(R.drawable.list_entropy));
			background.add(getResources().getDrawable(R.drawable.darkorange));
			icon.add(getResources().getDrawable(R.drawable.list_trim));
			background.add(getResources().getDrawable(R.drawable.royalblue));
			icon.add(getResources().getDrawable(R.drawable.list_zipalign));
			background.add(getResources().getDrawable(R.drawable.aquamarine));
			icon.add(getResources().getDrawable(R.drawable.list_sqlite3));
			background.add(getResources().getDrawable(R.drawable.firebrick));
			icon.add(getResources().getDrawable(R.drawable.list_all));
			background.add(getResources().getDrawable(R.drawable.mediumorchid));
			for (int i = 0; i < addon_title.size(); i++) {
				adapter.add(new Menu(addon_title.get(i), addon_subtitle.get(i), background.get(i), icon.get(i)));
			}
			return null;
		}
	}
}
