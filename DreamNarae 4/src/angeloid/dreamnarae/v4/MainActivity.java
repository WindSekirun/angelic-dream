package angeloid.dreamnarae.v4;

import com.crashlytics.android.Crashlytics;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import org.holoeverywhere.app.Activity;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import angeloid.Narae.NaraePreference;
import angeloid.dreamnarae.Fragment.EconomyFragment;
import angeloid.dreamnarae.Fragment.PagerAdapter;
import angeloid.dreamnarae.Fragment.TweakFragment;
import angeloid.dreamnarae.Shared.StackManager;
import angeloid.dreamnarae.Shared.StopWatchAverage;
import com.stericson.RootTools.RootTools;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint({ "NewApi", "HandlerLeak" })
public class MainActivity extends Activity {
	public PagerAdapter mPagerAdapter;
	public ViewPager pager;
	public ActionBar ab;
	public ActionBar.TabListener tabListener;
	public StackManager am = StackManager.getInstance();
	public long backPressedTime = 0;
	public ProgressDialog mDlg;
	public String output;
	public int check, resultcode;
	public ActionBar.Tab Economy, Tweak;
	public StopWatchAverage sw;
	public NaraePreference np;
	public static Activity a;
	public PackageInfo i;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		am.addActivity(this);
		Crouton.cancelAllCroutons();
		setContentView(R.layout.mainactivity);
		if (a == null) {
			a = MainActivity.this;
		}
		if (np == null) {
			np = new NaraePreference(this);
		}
		boolean root = RootTools.isAccessGiven();
		np.put("root", root);
		if (Locale.getDefault().getLanguage().equals("ko")) {
			np.put("lang", "ko");
		} else {
			np.put("lang", "en");
		}
		List<Fragment> fragments = new Vector<Fragment>();
		fragments.add(Fragment.instantiate(this, EconomyFragment.class.getName()));
		fragments.add(Fragment.instantiate(this, TweakFragment.class.getName()));
		this.mPagerAdapter = new PagerAdapter(super.getSupportFragmentManager(), fragments);
		pager = (ViewPager) super.findViewById(R.id.pager);
		pager.setAdapter(this.mPagerAdapter);
		ab = getSupportActionBar();
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		ab.setIcon(R.drawable.ic_actionbar);
		Economy = ab.newTab().setIcon(R.drawable.ic_economy_enable);
		Tweak = ab.newTab().setIcon(R.drawable.ic_tweak_disable);
		ab.addTab(Economy.setTabListener(new TabListener()));
		ab.addTab(Tweak.setTabListener(new TabListener()));
		pager.setOnPageChangeListener(new GoingListener());
		Intent i = getIntent();
		int value = i.getIntExtra("value", 0);
		if (value == 0) {
			ab.setSelectedNavigationItem(0);
			pager.setCurrentItem(0);
			Economy.setIcon(R.drawable.ic_economy_enable);
			Tweak.setIcon(R.drawable.ic_tweak_disable);
		} else if (value == 1) {
			ab.setSelectedNavigationItem(1);
			pager.setCurrentItem(1);
			Economy.setIcon(R.drawable.ic_economy_disable);
			Tweak.setIcon(R.drawable.ic_tweak_enable);
		}
	}

	public void DeleteAlert() {
		AlertDialog.Builder b = new AlertDialog.Builder(this);
		b.setMessage(getResources().getString(R.string.alert_delete));
		b.setPositiveButton(android.R.string.ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				np.put("mode", "null");
			}
		});
		b.setNegativeButton(android.R.string.no, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		b.show();
	}

	public class GoingListener implements ViewPager.OnPageChangeListener {
		@Override
		public void onPageSelected(int i) {
			if (i == 0) {
				Economy.setIcon(R.drawable.ic_economy_enable);
				Tweak.setIcon(R.drawable.ic_tweak_disable);
			} else if (i == 1) {
				Economy.setIcon(R.drawable.ic_economy_disable);
				Tweak.setIcon(R.drawable.ic_tweak_enable);
			}
			ab.setSelectedNavigationItem(i);
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}
	}

	public class TabListener implements ActionBar.TabListener {
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			int i = tab.getPosition();
			if (i == 0) {
				Economy.setIcon(R.drawable.ic_economy_enable);
				Tweak.setIcon(R.drawable.ic_tweak_disable);
			} else if (i == 1) {
				Economy.setIcon(R.drawable.ic_economy_disable);
				Tweak.setIcon(R.drawable.ic_tweak_enable);
			}
			pager.setCurrentItem(tab.getPosition());
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}

	@Override
	public void onDestroy() {
		am.finishAllActivity();
		super.onDestroy();
		Crouton.cancelAllCroutons();
	}

	@Override
	public void onBackPressed() {
		long tempTime = System.currentTimeMillis();
		long intervalTime = tempTime - backPressedTime;
		if (0 <= intervalTime && 2000 >= intervalTime) {
			Crouton.cancelAllCroutons();
			am.finishAllActivity();
			android.os.Process.killProcess(android.os.Process.myPid());
		} else {
			backPressedTime = tempTime;
			Crouton.makeText(MainActivity.this, getResources().getString(R.string.crouton_exit), Style.CONFIRM).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent i = new Intent(MainActivity.this, SettingsActivity.class);
			i.putExtra("value", pager.getCurrentItem());
			startActivity(i);
		} else if (id == R.id.action_delete) {
			DeleteAlert();
		}
		return super.onOptionsItemSelected(item);
	}
}
