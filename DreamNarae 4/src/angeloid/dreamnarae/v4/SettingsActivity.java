package angeloid.dreamnarae.v4;

import org.holoeverywhere.preference.CheckBoxPreference;
import org.holoeverywhere.preference.Preference;
import org.holoeverywhere.preference.PreferenceActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import angeloid.dreamnarae.Shared.StackManager;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

@SuppressLint("InlinedApi")
public class SettingsActivity extends PreferenceActivity {
	public StackManager am = StackManager.getInstance();
	public CheckBoxPreference bootnoti, allowreport;
	public Preference license, version, site, developer, sendreport;
	public PackageInfo i;
	public String versioninfo;
	public int value;
	public long[] mHits = new long[3];

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		am.addActivity(this);
		addPreferencesFromResource(R.xml.settings);
		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setHomeButtonEnabled(true);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowHomeEnabled(true);
		ab.setDisplayShowTitleEnabled(true);
		ab.setIcon(R.drawable.ic_actionbar);
		ab.setTitle(R.string.activity_preference);
		try {
			i = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {}
		versioninfo = i.versionName + " (#" + i.versionCode + ")";
		version = (Preference) findPreference("version");
		version.setSummary(versioninfo);
		version.setOnPreferenceClickListener(new Click_version());
		developer = (Preference) findPreference("developer");
		bootnoti = (CheckBoxPreference) findPreference("bootnoti");
		license = (Preference) findPreference("license");
		license.setOnPreferenceClickListener(new Click_license());
		site = (Preference) findPreference("site");
		site.setOnPreferenceClickListener(new Click_site());
		developer.setOnPreferenceClickListener(new Click_developer());
		Intent i = getIntent();
		value = i.getIntExtra("value", 0);
	}

	@Override
	public void onDestroy() {
		am.removeActivity(this);
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		Intent i = new Intent(this, MainActivity.class);
		i.putExtra("value", value);
		startActivity(i);;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			Intent i = new Intent(this, MainActivity.class);
			i.putExtra("value", value);
			startActivity(i);;
		}
		return super.onOptionsItemSelected(item);
	}

	public void WebView(String url) {
		AlertDialog.Builder b = new AlertDialog.Builder(SettingsActivity.this);
		WebView wv = new WebView(SettingsActivity.this);
		wv.loadUrl(url);
		wv.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});
		b.setView(wv);
		b.setPositiveButton(android.R.string.cancel, new OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				arg0.dismiss();
			}
		});
		b.show();
	}

	public class Click_version implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			if (Build.VERSION.SDK_INT >= 14) {
				System.arraycopy(mHits, 1, mHits, 0, mHits.length - 1);
				mHits[mHits.length - 1] = SystemClock.uptimeMillis();
				if (mHits[0] >= (SystemClock.uptimeMillis() - 500)) {
					Intent intent = new Intent(SettingsActivity.this, PlatLogoActivity.class);
					startActivity(intent);;
				}
			} else {
				Crouton.makeText(SettingsActivity.this, R.string.crouton_notneed, Style.ALERT).show();
			}
			return true;
		}
	}

	public class Click_site implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.angeloiddev.com"));
			startActivity(intent);
			return true;
		}
	}

	public class Click_license implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			Intent i = new Intent(SettingsActivity.this, LicenseActivity.class);
			startActivity(i);
			return true;
		}
	}

	public class Click_developer implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			Intent i = new Intent(SettingsActivity.this, DeveloperInfo.class);
			startActivity(i);
			return true;
		}
	}
}
