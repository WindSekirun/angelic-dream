package angeloid.dreamnarae.v4;

import com.crashlytics.android.Crashlytics;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.StrictMode;
import android.util.Log;
import angeloid.Narae.NaraeFiles;
import angeloid.Narae.NaraePreference;
import angeloid.Narae.NaraeTools;

@SuppressLint("NewApi")
public class Config_Build {
	protected final String MODE_DEVELOPER = "DEVELOPER MODE";
	protected final String MODE_DEBUG = "DEBUGGING MODE";
	protected final String MODE_USER = "USER MODE";
	protected final String DEV_SCREEN = "developerscreen";
	protected final String AUTO_REPORT = "autoreport";
	protected final String LOGTAG = "DreamNarae";
	protected static Context c;
	protected static Config_Build cb;
	protected static NaraeTools nt;
	protected NaraeFiles nf = NaraeFiles.getInstance();
	protected static NaraePreference np;
	/*
	 * PI_MODE
	 * @param 0 - market release 1 - user debugging 2 - developer mode
	 */
	public int PI_MODE = 2;

	private Config_Build() {
	}

	public static Config_Build getInstance(Context mc) {
		if (cb == null) {
			cb = new Config_Build();
		}
		c = mc;
		nt = NaraeTools.getInstance(c);
		np = new NaraePreference(c);
		return cb;
	}

	public void Init() {
		Log.d(LOGTAG, "WELCOME");
		if (0 == PI_MODE) {
			User();
		} else if (PI_MODE == 1) {
			Debug();
		} else if (PI_MODE == 2) {
			Developer();
		}
		if (np.getValue(AUTO_REPORT, true) == true) {
			Crashlytics.start(c);
		}
	}

	private void User() {
		np.put(DEV_SCREEN, false);
	}

	private void Debug() {
		np.put(DEV_SCREEN, true);
		unStrict();
	}

	private void Developer() {
		np.put(DEV_SCREEN, true);
		np.put(AUTO_REPORT, true);
		unStrict();
	}

	private void unStrict() {
		StrictMode.ThreadPolicy stp = new StrictMode.ThreadPolicy.Builder().detectAll().penaltyFlashScreen().penaltyLog().build();
		StrictMode.setThreadPolicy(stp);
		StrictMode.VmPolicy svp = new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().penaltyDeath().build();
		StrictMode.setVmPolicy(svp);
	}
}
