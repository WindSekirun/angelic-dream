package angeloid.dreamnarae.v4;

import org.holoeverywhere.app.Application;

public class DreamNarae extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		Config_Build cb = Config_Build.getInstance(this);
		cb.Init();
	}
}
