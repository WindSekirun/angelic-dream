package angeloid.dreamnarae.v4;

import java.util.List;
import java.util.Vector;
import org.holoeverywhere.app.Activity;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.view.MenuItem;
import angeloid.dreamnarae.Fragment.DisableFragment;
import angeloid.dreamnarae.Fragment.EnableFragment;
import angeloid.dreamnarae.Fragment.PagerAdapter;
import angeloid.dreamnarae.Shared.StackManager;
import de.keyboardsurfer.android.widget.crouton.Crouton;

@SuppressLint("InlinedApi")
public class DisablerActivity extends Activity {
	public StackManager am = StackManager.getInstance();
	private PagerAdapter mPagerAdapter;
	public ViewPager pager;
	public ActionBar ab;
	public ActionBar.TabListener tabListener;
	public ActionBar.Tab Disable;
	public ActionBar.Tab Enable;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		am.addActivity(this);
		Crouton.cancelAllCroutons();
		setContentView(R.layout.disableactivity);
		List<Fragment> fragments = new Vector<Fragment>();
		fragments.add(Fragment.instantiate(this, DisableFragment.class.getName()));
		fragments.add(Fragment.instantiate(this, EnableFragment.class.getName()));
		this.mPagerAdapter = new PagerAdapter(super.getSupportFragmentManager(), fragments);
		pager = (ViewPager) super.findViewById(R.id.pager);
		pager.setAdapter(this.mPagerAdapter);
		ab = getSupportActionBar();
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		ab.setIcon(R.drawable.ic_actionbar);
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setHomeButtonEnabled(true);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowHomeEnabled(true);
		ab.setDisplayShowTitleEnabled(true);
		ab.setDisplayUseLogoEnabled(true);
		ab.setTitle(getResources().getString(R.string.activity_disabler));
		Disable = ab.newTab().setText(R.string.tab_disable);
		Enable = ab.newTab().setText(R.string.tab_enable);
		ab.addTab(Disable.setTabListener(new TabListener()));
		ab.addTab(Enable.setTabListener(new TabListener()));
		pager.setOnPageChangeListener(new GoingListener());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			super.onBackPressed();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDestroy() {
		am.removeActivity(this);
		Crouton.clearCroutonsForActivity(this);
		super.onDestroy();
	}

	public class GoingListener implements ViewPager.OnPageChangeListener {
		@Override
		public void onPageSelected(int i) {
			ab.setSelectedNavigationItem(i);
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}
	}

	public class TabListener implements ActionBar.TabListener {
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			pager.setCurrentItem(tab.getPosition());
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}
}
