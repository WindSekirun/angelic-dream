package angeloid.dreamnarae.v4;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import org.holoeverywhere.preference.Preference;
import org.holoeverywhere.preference.PreferenceActivity;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import angeloid.Narae.NaraePreference;
import angeloid.Narae.NaraeTools;
import angeloid.dreamnarae.Shared.StackManager;
import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;
import com.stericson.RootTools.execution.CommandCapture;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("InlinedApi")
public class EntropyActivity extends PreferenceActivity {
	public Preference service_passive;
	public Preference service_entropy;
	public Preference service_last;
	public Preference service_status;
	public NaraePreference dn;
	public StackManager am = StackManager.getInstance();
	public NaraeTools nt = NaraeTools.getInstance(this);

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle s) {
		super.onCreate(s);
		am.addActivity(this);
		addPreferencesFromResource(R.xml.entropy);
		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setHomeButtonEnabled(true);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowHomeEnabled(true);
		ab.setDisplayShowTitleEnabled(true);
		ab.setTitle(R.string.activity_entropy);
		ab.setIcon(R.drawable.ic_actionbar);
		dn = new NaraePreference(EntropyActivity.this);
		service_status = (Preference) findPreference("service_status");
		service_passive = (Preference) findPreference("service_passive");
		service_entropy = (Preference) findPreference("service_entropy");
		service_last = (Preference) findPreference("service_last");
		service_last.setSummary(dn.getValue("service_last_entropy", "null"));
		service_entropy.setOnPreferenceClickListener(new Click_Info());
		service_passive.setOnPreferenceClickListener(new Click_entropy());
		service_status.setOnPreferenceClickListener(new Click_status());
		if (new File("/system/bin/narae").exists() == false) {
			AlertDialog.Builder d = new Builder(this);
			d.setMessage(getString(R.string.crouton_binary));
			d.setPositiveButton(android.R.string.cancel, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
		}
	}

	@Override
	public void onDestroy() {
		am.removeActivity(this);
		Crouton.clearCroutonsForActivity(this);
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			super.onBackPressed();;
		}
		return super.onOptionsItemSelected(item);
	}

	public class Click_entropy implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference p) {
			dn.put("service_last_entropy", nt.getTime());
			Crouton.makeText(EntropyActivity.this, getResources().getString(R.string.crouton_done), Style.INFO).show();
			service_last.setSummary(dn.getValue("service_last_entropy", "null"));
			return true;
		}
	}

	public class Click_status implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference p) {
			AlertDialog.Builder d = new AlertDialog.Builder(EntropyActivity.this);
			View v = getLayoutInflater().inflate(R.layout.popupentropy);
			TextView tv = (TextView) v.findViewById(R.id.value);
			if (new File("/proc/sys/kernel/random/entropy_avail").exists()) {
				try {
					tv.setText(CheckEntropy());
				} catch (IOException e) {} catch (TimeoutException e) {} catch (RootDeniedException e) {}
			} else {
				tv.setText(R.string.entropy_kernal);
			}
			d.setView(v);
			d.setPositiveButton(android.R.string.cancel, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			d.show();
			return true;
		}
	}

	public class Click_Info implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			AlertDialog.Builder b = new AlertDialog.Builder(EntropyActivity.this);
			WebView wv = new WebView(EntropyActivity.this);
			if (dn.getValue("lang", "en").equals("ko")) {
				wv.loadUrl("file:///android_asset/service/entropy_ko.html");
			} else {
				wv.loadUrl("file:///android_asset/service/entropy.html");
			}
			wv.setWebViewClient(new WebViewClient() {
				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					view.loadUrl(url);
					return true;
				}
			});
			b.setView(wv);
			b.setPositiveButton(android.R.string.cancel, new OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					arg0.dismiss();
				}
			});
			b.show();
			return true;
		}
	}

	public String CheckEntropy() throws IOException, TimeoutException, RootDeniedException {
		CommandCapture c = new CommandCapture(0, "cat /proc/sys/kernel/random/entropy_avail > "
				+ Environment.getExternalStorageDirectory().getAbsolutePath() + "/entropy.txt");
		RootTools.getShell(true).add(c);
		String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
		String fileName = "entropy.txt";
		FileInputStream inputStream = new FileInputStream(baseDir + File.separator + fileName);
		byte[] data = new byte[inputStream.available()];
		while (inputStream.read(data) != -1) {
			;
		}
		inputStream.close();
		String list = new String(data);
		return list;
	}
}
