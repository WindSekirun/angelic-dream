package angeloid.dreamnarae.v4;

import java.util.ArrayList;
import org.holoeverywhere.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import angeloid.Narae.NaraePreference;
import angeloid.dreamnarae.Shared.StackManager;
import angeloid.dreamnarae.Shared.TweakDBAdapter;
import angeloid.dreamnarae.v4.R;

public class TweakEdit extends Activity {
	public ListView Static;
	public ListView recent;
	public ArrayList<Menu> pd;
	public ListAdapter adapter;
	public StackManager am = StackManager.getInstance();
	public View header;
	public View header_recent;
	public View v;
	public TextView header_menu;
	public TextView recentchanges;
	public NaraePreference dn;
	public TweakDBAdapter mDbHelper;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		am.addActivity(this);
		setContentView(R.layout.menulist);
		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setHomeButtonEnabled(true);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowHomeEnabled(true);
		ab.setDisplayShowTitleEnabled(true);
		ab.setDisplayUseLogoEnabled(true);
		ab.setIcon(R.drawable.ic_actionbar);
		ab.setTitle(R.string.add_tweak);
		mDbHelper = new TweakDBAdapter(this);
		mDbHelper.open();
		pd = new ArrayList<Menu>();
		adapter = new ListAdapter(this, pd);
		Static = (ListView) findViewById(R.id.listView1);
		header = getLayoutInflater().inflate(R.layout.section_divider);
		header_menu = (TextView) header.findViewById(R.id.headertext);
		header_menu.setText(R.string.add_header_menu);
		Static.addHeaderView(header);
		header_recent = getLayoutInflater().inflate(R.layout.section_divider);
		recentchanges = (TextView) header_recent.findViewById(R.id.headertext);
		recentchanges.setText(R.string.add_header_recent);
		v = getLayoutInflater().inflate(R.layout.menulist);
		recent = (ListView) v.findViewById(R.id.listView1);
		recent.addHeaderView(recentchanges);
		Static.addFooterView(v);
		new LoadData().execute();
		new LoadRecentData().execute();
	}

	public class LoadRecentData extends AsyncTask<Void, Void, Void> {
		@SuppressWarnings("deprecation")
		@Override
		protected Void doInBackground(Void... params) {
			Cursor tweakCursor = mDbHelper.fetchAllFileLog();
			startManagingCursor(tweakCursor);
			String[] file = new String[] { TweakDBAdapter.KEY_FILE };
			String[] time = new String[] { TweakDBAdapter.KEY_TIME };
			pd = new ArrayList<Menu>();
			adapter = new ListAdapter(TweakEdit.this, pd);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}
	}

	public class LoadData extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... arg0) {
			adapter.add(new Menu(getResources().getString(R.string.add_tweak), null));
			adapter.add(new Menu(getResources().getString(R.string.add_prev), null));
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			Static.setAdapter(adapter);
			Static.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
					if (arg2 == 0) {
						Intent i = new Intent(TweakEdit.this, EditActivity.class);
						i.putExtra("value", 1);
						startActivity(i);
					} else if (arg2 == 1) {
						Intent i = new Intent(TweakEdit.this, EditActivity.class);
						i.putExtra("value", 1);
						startActivity(i);
					}
				}
			});
			super.onPostExecute(result);
		}
	}

	public class ListAdapter extends ArrayAdapter<Menu> {
		public LayoutInflater inflater;

		public ListAdapter(Context c, ArrayList<Menu> o) {
			super(c, 0, o);
			inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getView(int position, View v, ViewGroup parent) {
			View view = null;
			if (v == null) {
				view = inflater.inflate(R.layout.row_recent, null);
			} else {
				view = v;
			}
			final Menu data = this.getItem(position);
			TextView title = (TextView) view.findViewById(R.id.Title);
			TextView subtext = (TextView) view.findViewById(R.id.subtext);
			title.setText(data.getTitle());
			subtext.setText(data.getSubText());
			return view;
		}
	}

	public class Menu {
		String Title;
		String SubText;

		public Menu(String t, String p) {
			Title = t;
			SubText = p;
		}

		public String getTitle() {
			return Title;
		}

		public String getSubText() {
			return SubText;
		}
	}
}
