package angeloid.dreamnarae.v4;

import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import org.holoeverywhere.app.AlertDialog;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import angeloid.dreamnarae.Base.BaseListViewActivity;
import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.execution.CommandCapture;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

@SuppressLint("InlinedApi")
public class DiskActivity extends BaseListViewActivity {
	public ActionBar.TabListener tabListener;
	public ListView list;
	public static ArrayList<String> disk_title;
	public static ArrayList<String> disk_subtitle;
	public ProgressDialog mDlg;
	public String mDispPattern = "0";
	public DecimalFormat mForm;

	@Override
	public void onCreate(Bundle s) {
		super.onCreate(s);
		am.addActivity(this);
		setContentView(R.layout.menulist);
		init();
		list = (ListView) findViewById(R.id.listView1);
		View header = getLayoutInflater().inflate(R.layout.disk, null);
		list.addHeaderView(header);
		new LoadData().execute();
		ab.addTab(Economy.setTabListener(new TabListener()));
		ab.addTab(Tweak.setTabListener(new TabListener()));
	}

	public class ClickMenu implements AdapterView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			if (position == 1) {
				startActivityForResult(new Intent(android.provider.Settings.ACTION_MEMORY_CARD_SETTINGS), 0);
			} else if (position == 2) {
				AlertDialog.Builder builder = new AlertDialog.Builder(DiskActivity.this);
				builder.setMessage(getString(R.string.alert_cache));
				builder.setPositiveButton(android.R.string.yes, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						new ClearCache().execute();
						Crouton.makeText(DiskActivity.this, getResources().getString(R.string.crouton_done), Style.INFO).show();
					}
				});
				builder.setNegativeButton(android.R.string.no, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				builder.show();
			} else if (position == 3) {
				AlertDialog.Builder builder = new AlertDialog.Builder(DiskActivity.this);
				builder.setMessage(getString(R.string.alert_thumb));
				builder.setPositiveButton(android.R.string.yes, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						new DeleteTemp().execute();
						Crouton.makeText(DiskActivity.this, getResources().getString(R.string.crouton_done), Style.INFO).show();
					}
				});
				builder.setNegativeButton(android.R.string.no, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				builder.show();
			}
		}
	}

	public class TabListener implements ActionBar.TabListener {
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			int i = tab.getPosition();
			if (i == 0) {} else if (i == 1) {
				Economy.setIcon(R.drawable.ic_economy_disable);
				Tweak.setIcon(R.drawable.ic_tweak_enable);
				Intent a = new Intent(DiskActivity.this, MainActivity.class);
				a.putExtra("value", 1);
				startActivity(a);
			}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}

	public class LoadData extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPostExecute(Void result) {
			list.setAdapter(adapter);
			list.setOnItemClickListener(new ClickMenu());
			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			disk_title = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.disk_menu_title)));
			disk_subtitle = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.disk_menu_subtitle)));
			ArrayList<Drawable> icon = new ArrayList<Drawable>();
			ArrayList<Drawable> background = new ArrayList<Drawable>();
			icon.add(getResources().getDrawable(R.drawable.list_diskinfo));
			icon.add(getResources().getDrawable(R.drawable.list_cache));
			icon.add(getResources().getDrawable(R.drawable.list_temp));
			background.add(getResources().getDrawable(R.drawable.aquamarine));
			background.add(getResources().getDrawable(R.drawable.darkorange));
			background.add(getResources().getDrawable(R.drawable.mediumorchid));
			for (int i = 0; i < disk_title.size(); i++) {
				adapter.add(new Menu(disk_title.get(i), disk_subtitle.get(i), background.get(i), icon.get(i)));
			}
			return null;
		}
	}

	public class DeleteTemp extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... arg0) {
			CommandCapture command = new CommandCapture(0, "cd " + Environment.getExternalStorageDirectory() + "/DCIM", "busybox rm thumb",
					"mv .thumbnails thumb", "busybox rm thumb");
			try {
				RootTools.getShell(false).add(command);
			} catch (Exception e) {}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			mDlg.dismiss();
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			mDlg = new ProgressDialog(DiskActivity.this);
			mDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mDlg.setMessage(getResources().getString(R.string.progress_apply));
			mDlg.setIndeterminate(false);
			mDlg.setCancelable(true);
			mDlg.setCanceledOnTouchOutside(false);
			mDlg.show();
			super.onPreExecute();
		}
	}

	public class ClearCache extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... arg0) {
			PackageManager pm = getPackageManager();
			Method[] methods = pm.getClass().getDeclaredMethods();
			for (Method m : methods) {
				if (m.getName().equals("freeStorageAndNotify")) {
					try {
						m.invoke(pm, Long.MAX_VALUE, null);
					} catch (Exception e) {}
					break;
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			mDlg.dismiss();
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			mDlg = new ProgressDialog(DiskActivity.this);
			mDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mDlg.setMessage(getResources().getString(R.string.progress_apply));
			mDlg.setIndeterminate(false);
			mDlg.setCancelable(true);
			mDlg.setCanceledOnTouchOutside(false);
			mDlg.show();
			super.onPreExecute();
		}
	}
}