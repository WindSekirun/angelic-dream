package angeloid.dreamnarae.v4;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.holoeverywhere.app.AlertDialog;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Browser;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import angeloid.dreamnarae.Base.BaseListViewActivity;
import angeloid.dreamnarae.Shared.StackManager;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
@SuppressWarnings("deprecation")
public class HistoryActivity extends BaseListViewActivity {

	public ActionBar ab;
	public ActionBar.TabListener tabListener;
	public ListView list;
	public ArrayList<Menu> pd;
	public StackManager am = StackManager.getInstance();
	public ListAdapter adapter;
	public List<String> title;
	public static ArrayList<String> history_title;
	public List<String> subtitle;
	public static ArrayList<String> history_subtitle;
	public TypedArray ar_icon;
	public int len_icon;
	public int[] picArray_icon;
	public TypedArray ar_background;
	public int len_background;
	public int[] picArray_background;
	public ActionBar.Tab Economy;
	public ActionBar.Tab Tweak;
	public ProgressDialog mDlg;

	@Override
	public void onCreate(Bundle s) {
		super.onCreate(s);
		am.addActivity(this);
		setContentView(R.layout.menulist);
		list = (ListView) findViewById(R.id.listView1);
		View header = getLayoutInflater().inflate(R.layout.history, null);
		list.addHeaderView(header);
		pd = new ArrayList<Menu>();
		adapter = new ListAdapter(this, pd);
		new LoadData().execute();
		ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setHomeButtonEnabled(true);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowHomeEnabled(true);
		ab.setDisplayShowTitleEnabled(true);
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		Economy = ab.newTab().setIcon(R.drawable.ic_economy_enable);
		ab.setIcon(R.drawable.ic_actionbar);
		Tweak = ab.newTab().setIcon(R.drawable.ic_tweak_disable);
		ab.addTab(Economy.setTabListener(new TabListener()));
		ab.addTab(Tweak.setTabListener(new TabListener()));
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			Intent i = new Intent(this, MainActivity.class);
			startActivity(i);
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onDestroy() {
		am.removeActivity(this);
		Crouton.clearCroutonsForActivity(this);
		super.onDestroy();
	}

	public boolean isDetected(Context c, String strPackageName) {
		PackageInfo pi = null;
		try {
			pi = c.getPackageManager().getPackageInfo(strPackageName, 0);
		} catch (NameNotFoundException e) {
		}
		if (pi != null) {
			return true;
		}
		return false;
	}

	public class ClickMenu implements AdapterView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			if (position == 1) {
				AlertDialog.Builder builder = new AlertDialog.Builder(HistoryActivity.this);
				builder.setMessage(getString(R.string.alert_run));
				builder.setPositiveButton(android.R.string.yes, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						if (isDetected(HistoryActivity.this, "com.android.browser")) {
							try {
								Browser.clearHistory(getContentResolver());
								Crouton.makeText(HistoryActivity.this, getResources().getString(R.string.crouton_done), Style.INFO).show();
							} catch (Exception e) {

							}
						} else {
							Crouton.makeText(HistoryActivity.this, getResources().getString(R.string.crouton_notneed), Style.ALERT).show();
						}
					}
				});
				builder.setNegativeButton(android.R.string.no, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();

					}
				});
				builder.show();
			} else if (position == 2) {
				AlertDialog.Builder builder = new AlertDialog.Builder(HistoryActivity.this);
				builder.setMessage(getString(R.string.alert_run));
				builder.setPositiveButton(android.R.string.yes, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
							ClipboardManager clipBoard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
							ClipData data = ClipData.newPlainText("", "");
							clipBoard.setPrimaryClip(data);
						} else {
							android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
							clipboard.setText("");
						}
						Crouton.makeText(HistoryActivity.this, getResources().getString(R.string.crouton_done), Style.INFO).show();
					}
				});
				builder.setNegativeButton(android.R.string.no, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();

					}
				});
				builder.show();

			} else if (position == 3) {
				AlertDialog.Builder builder = new AlertDialog.Builder(HistoryActivity.this);
				builder.setMessage(getString(R.string.alert_run));
				builder.setPositiveButton(android.R.string.yes, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						SearchRecentSuggestions mapsrs = new SearchRecentSuggestions(HistoryActivity.this.getBaseContext(),
								"com.google.android.maps.SearchHistoryProvider", 1);
						mapsrs.clearHistory();
					}
				});
				builder.setNegativeButton(android.R.string.no, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();

					}
				});
				builder.show();
			} else if (position == 4) {
				AlertDialog.Builder builder = new AlertDialog.Builder(HistoryActivity.this);
				builder.setMessage(getString(R.string.alert_run));
				builder.setPositiveButton(android.R.string.yes, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						new ClearLog().execute();
					}
				});
				builder.setNegativeButton(android.R.string.no, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();

					}
				});
				builder.show();
			}
		}
	}

	public class TabListener implements ActionBar.TabListener {

		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			int i = tab.getPosition();
			if (i == 0) {
			} else if (i == 1) {
				Economy.setIcon(R.drawable.ic_economy_disable);
				Tweak.setIcon(R.drawable.ic_tweak_enable);
				Intent a = new Intent(HistoryActivity.this, MainActivity.class);
				a.putExtra("value", 1);
				startActivity(a);
				;
			}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}

	public class LoadData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPostExecute(Void result) {
			list.setAdapter(adapter);
			list.setOnItemClickListener(new ClickMenu());
			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			title = Arrays.asList(getResources().getStringArray(R.array.history_menu_title));
			history_title = new ArrayList<String>(title);
			subtitle = Arrays.asList(getResources().getStringArray(R.array.history_menu_subtitle));
			history_subtitle = new ArrayList<String>(subtitle);
			ArrayList<Drawable> icon = new ArrayList<Drawable>();
			ArrayList<Drawable> background = new ArrayList<Drawable>();
			icon.add(getResources().getDrawable(R.drawable.list_browser));
			icon.add(getResources().getDrawable(R.drawable.list_clipboard));
			icon.add(getResources().getDrawable(R.drawable.list_map));
			icon.add(getResources().getDrawable(R.drawable.list_log));
			background.add(getResources().getDrawable(R.drawable.aquamarine));
			background.add(getResources().getDrawable(R.drawable.royalblue));
			background.add(getResources().getDrawable(R.drawable.firebrick));
			background.add(getResources().getDrawable(R.drawable.darkorange));
			for (int i = 0; i < history_title.size(); i++) {
				adapter.add(new Menu(history_title.get(i), history_subtitle.get(i), background.get(i), icon.get(i)));
			}
			return null;
		}
	}

	public class ClearLog extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			try {
				String fileName = "*.log";
				Collection<File> files = FileUtils.listFiles(Environment.getExternalStorageDirectory().getAbsoluteFile(), null, true);

				for (Iterator<File> iterator = files.iterator(); iterator.hasNext();) {
					File file = (File) iterator.next();
					if (file.getName().equals(fileName)) file.delete();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			mDlg.dismiss();
			Crouton.makeText(HistoryActivity.this, getResources().getString(R.string.crouton_done), Style.INFO).show();
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			mDlg = new ProgressDialog(HistoryActivity.this);
			mDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mDlg.setIndeterminate(false);
			mDlg.setCancelable(true);
			mDlg.setMessage(getResources().getString(R.string.progress_work));
			mDlg.setCanceledOnTouchOutside(false);
			mDlg.show();
			super.onPreExecute();
		}
	}
}
