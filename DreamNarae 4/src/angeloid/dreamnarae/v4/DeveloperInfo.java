package angeloid.dreamnarae.v4;

import java.util.ArrayList;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.ListView;
import angeloid.dreamnarae.Base.BaseListViewActivity;
import angeloid.dreamnarae.Shared.StackManager;
import de.keyboardsurfer.android.widget.crouton.Crouton;

public class DeveloperInfo extends BaseListViewActivity {
	public StackManager am = StackManager.getInstance();
	public ActionBar ab;
	public ActionBar.TabListener tabListener;
	public ListView list;
	public ArrayList<Menu> pd;
	public ListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		am.addActivity(this);
		setContentView(R.layout.menulist);
		list = (ListView) findViewById(R.id.listView1);
		pd = new ArrayList<Menu>();
		adapter = new ListAdapter(DeveloperInfo.this, pd);
		ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setHomeButtonEnabled(true);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowHomeEnabled(true);
		ab.setDisplayShowTitleEnabled(true);
		ab.setIcon(R.drawable.ic_actionbar);
		ab.setTitle(R.string.activity_developer);
		new LoadData().execute();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			Intent i = new Intent(this, SettingsActivity.class);
			startActivity(i);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();;
	}

	public void onDestroy() {
		am.removeActivity(this);
		Crouton.cancelAllCroutons();
		super.onDestroy();
	}

	public class LoadData extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			ArrayList<String> title = new ArrayList<String>();
			ArrayList<String> subtitle = new ArrayList<String>();
			ArrayList<Drawable> icon = new ArrayList<Drawable>();
			ArrayList<Drawable> background = new ArrayList<Drawable>();
			title.add(getString(R.string.list_developer_produce));
			subtitle.add(getString(R.string.info_developer_produce));
			background.add(getResources().getDrawable(R.drawable.mediumorchid));
			icon.add(getResources().getDrawable(R.drawable.list_p));
			title.add(getString(R.string.list_main_developer));
			subtitle.add(getString(R.string.info_main_developer));
			background.add(getResources().getDrawable(R.drawable.royalblue));
			icon.add(getResources().getDrawable(R.drawable.list_d));
			title.add(getString(R.string.list_designer));
			subtitle.add(getString(R.string.info_designer));
			background.add(getResources().getDrawable(R.drawable.aquamarine));
			icon.add(getResources().getDrawable(R.drawable.list_d));
			title.add(getString(R.string.list_tweaks));
			subtitle.add(getString(R.string.info_tweaks));
			background.add(getResources().getDrawable(R.drawable.darkorange));
			icon.add(getResources().getDrawable(R.drawable.list_t));
			title.add(getString(R.string.list_marketing));
			subtitle.add(getString(R.string.info_marketing));
			background.add(getResources().getDrawable(R.drawable.firebrick));
			icon.add(getResources().getDrawable(R.drawable.list_m));
			title.add(getString(R.string.list_thanksto));
			subtitle.add(getString(R.string.info_thanksto));
			background.add(getResources().getDrawable(R.drawable.dimgray));
			icon.add(getResources().getDrawable(R.drawable.list_t));
			for (int i = 0; i < title.size(); i++) {
				adapter.add(new Menu(title.get(i), subtitle.get(i), background.get(i), icon.get(i)));
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			list.setAdapter(adapter);
			super.onPostExecute(result);
		}
	}
}
