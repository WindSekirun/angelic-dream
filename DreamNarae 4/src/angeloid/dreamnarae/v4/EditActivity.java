package angeloid.dreamnarae.v4;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.preference.ListPreference;
import org.holoeverywhere.preference.Preference;
import org.holoeverywhere.preference.PreferenceActivity;
import org.holoeverywhere.widget.SeekBar;
import org.holoeverywhere.widget.SeekBar.OnSeekBarChangeListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import angeloid.LilyAngels.Tweak.Battery.Save_CpuLevel;
import angeloid.LilyAngels.Tweak.Battery.Save_SDCard;
import angeloid.Narae.NaraeFiles;
import angeloid.Narae.NaraePreference;
import angeloid.Narae.NaraeTools;
import angeloid.dreamnarae.Shared.StackManager;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class EditActivity extends PreferenceActivity {
	public ListPreference debug, video, window, entropy, dalvik, sdcard, cpu, bus, drop,dns;
	public Preference swappiness;
	public NaraePreference dn;
	public StackManager am = StackManager.getInstance();
	public TextView tv;
	public View v;
	public SeekBar sb;
	public AlertDialog.Builder builder;
	public ProgressDialog mDlg;
	public int prog;
	public OutputStream myOutput;
	public NaraeFiles nf = NaraeFiles.getInstance();
	public NaraeTools nt = NaraeTools.getInstance(this);

	@SuppressWarnings("deprecation")
	public void onCreate(Bundle s) {
		super.onCreate(s);
		am.addActivity(this);
		addPreferencesFromResource(R.xml.edittweak);
		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setHomeButtonEnabled(true);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowHomeEnabled(true);
		ab.setDisplayShowTitleEnabled(true);
		ab.setDisplayUseLogoEnabled(true);
		ab.setIcon(R.drawable.ic_actionbar);
		ab.setTitle(R.string.activity_edit);
		dn = new NaraePreference(EditActivity.this);
		debug = (ListPreference) findPreference("debug");
		setItemSummary(debug, dn.getValue("debug", getString(R.string.select_debug1)), getString(R.string.select_debug1));
		video = (ListPreference) findPreference("video");
		setItemSummary(video, dn.getValue("video", getString(R.string.select_video1)), getString(R.string.select_video1));
		window = (ListPreference) findPreference("window");
		window.setDefaultValue(240);
		setItemSummary(window, dn.getValue("window", getString(R.string.select_video2)), getString(R.string.select_video2));
		entropy = (ListPreference) findPreference("entropy_scope");
		setItemSummary(entropy, dn.getValue("entropy_scope", 80), getString(R.string.select_entropy2));
		swappiness = (Preference) findPreference("swappiness");
		swappiness.setOnPreferenceClickListener(new Click_swappiness());
		setItemSummary(swappiness, dn.getValue("swappiness", 60), 60);
		dalvik = (ListPreference) findPreference("dalvik");
		setItemSummary(dalvik, dn.getValue("dalvik", getString(R.string.select_dalvik2)), getString(R.string.select_dalvik2));
		sdcard = (ListPreference) findPreference("sdcard");
		setItemSummary(sdcard, dn.getValue("sdcard", 2048), "2048KB");
		cpu = (ListPreference) findPreference("cpu");
		setItemSummary(cpu, dn.getValue("cpu", getString(R.string.select_cpu1)), getString(R.string.select_cpu1));
		bus = (ListPreference) findPreference("bus");
		setItemSummary(bus, dn.getValue("bus", getString(R.string.select_cpu1)), getString(R.string.select_cpu1));
		drop = (ListPreference) findPreference("drop");
		setItemSummary(drop, dn.getValue("drop", getString(R.string.select_cpu1)), getString(R.string.select_cpu1));
		dns = (ListPreference) findPreference("dns");
		setItemSummary(dns, dn.getValue("dns", getString(R.string.select_video2)), getString(R.string.select_video2));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			AlertDialog.Builder ab = new AlertDialog.Builder(EditActivity.this);
			ab.setMessage(R.string.edit_save);
			ab.setPositiveButton(android.R.string.ok, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					new SaveData().execute();
					Crouton.makeText(EditActivity.this, getResources().getString(R.string.crouton_done), Style.INFO).show();
					Intent i = new Intent(EditActivity.this, MainActivity.class);
					i.putExtra("value", 1);
					startActivity(i);
				}
			});
			ab.setNegativeButton(android.R.string.no, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					Intent i = new Intent(EditActivity.this, MainActivity.class);
					i.putExtra("value", 1);
					startActivity(i);
				}
			});
			ab.show();
		}
		return super.onOptionsItemSelected(item);
	}

	public class Click_swappiness implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			builder = new AlertDialog.Builder(EditActivity.this);
			v = getLayoutInflater().inflate(R.layout.editseek);
			sb = (SeekBar) v.findViewById(R.id.seekBar1);
			tv = (TextView) v.findViewById(R.id.textView1);
			tv.setText(getString(R.string.edit_now) + " : " + String.valueOf(60));
			sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
				}

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					tv.setText(getString(R.string.edit_now) + " : " + String.valueOf(progress));
					prog = progress;
				}
			});
			builder.setView(v);
			builder.setTitle(R.string.edit_title);
			builder.setPositiveButton(android.R.string.ok, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dn.put("swappiness", prog);
				}
			});
			builder.show();
			return true;
		}
	}

	public void setItemSummary(ListPreference lp, String nowValue, String recommandValue) {
		lp.setSummary("[" + getString(R.string.edit_now) + ": " + nowValue + " ]" + " [" + getString(R.string.edit_recommand) + ": "
				+ recommandValue + " ]");
	}

	public void setItemSummary(ListPreference lp, int nowValue, int recommandValue) {
		lp.setSummary("[" + getString(R.string.edit_now) + ": " + nowValue + " ]" + " [" + getString(R.string.edit_recommand) + ": "
				+ recommandValue + " ]");
	}

	public void setItemSummary(Preference p, int nowValue, int recommandValue) {
		p.setSummary("[" + getString(R.string.edit_now) + ": " + nowValue + " ]" + " [" + getString(R.string.edit_recommand) + ": "
				+ recommandValue + " ]");
	}

	public void setItemSummary(ListPreference lp, int nowValue, String recommandValue) {
		lp.setSummary("[" + getString(R.string.edit_now) + ": " + nowValue + " ]" + " [" + getString(R.string.edit_recommand) + ": "
				+ recommandValue + " ]");
	}

	@Override
	public void onDestroy() {
		am.removeActivity(this);
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder ab = new AlertDialog.Builder(EditActivity.this);
		ab.setMessage(R.string.edit_save);
		ab.setPositiveButton(android.R.string.ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				new SaveData().execute();
				Crouton.makeText(EditActivity.this, getResources().getString(R.string.crouton_done), Style.INFO).show();
				Intent i = new Intent(EditActivity.this, MainActivity.class);
				i.putExtra("value", 1);
				finish();
			}
		});
		ab.setNegativeButton(android.R.string.no, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				Intent i = new Intent(EditActivity.this, MainActivity.class);
				i.putExtra("value", 1);
				finish();
			}
		});
		ab.show();
	}

	public class SaveData extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... arg0) {
			try {
				StringBuilder sb = new StringBuilder();
				sb.append("setprop debug.composition.type " + dn.getValue("debug", getString(R.string.select_dalvik2)));
				sb.append(System.getProperty("line.separator"));
				sb.append("setprop windowsmgr.max_events_per_sec " + dn.getValue("window", 240));
				sb.append(System.getProperty("line.separator"));
				if (dn.getValue("video", "false").equals("true")) {
					sb.append("setprop video.accelerate.hw 1");
					sb.append(System.getProperty("line.separator"));
				} else {
					sb.append("setprop video.accelerate.hw 0");
					sb.append(System.getProperty("line.separator"));
				}
				sb.append("	echo " + dn.getValue("swappiness", 60) + " > /proc/sys/vm/swappiness");
				sb.append(System.getProperty("line.separator"));
				sb.append(Save_CpuLevel.getEditTweak(dn.getValue("cpu", 5)));
				sb.append(System.getProperty("line.separator"));
				sb.append(Save_CpuLevel.getEditTweak(dn.getValue("bus", 3)));
				sb.append(System.getProperty("line.separator"));
				sb.append("setprop dalvik.vm.execution-mode int:" + dn.getValue("dalvik", "jit"));
				sb.append(System.getProperty("line.separator"));
				if (!(dn.getValue("sdcard", String.valueOf(2048)).equals("null"))) {
					sb.append(Save_SDCard.getEditTweak(dn.getValue("sdcard", 2048)));
					sb.append(System.getProperty("line.separator"));
				}
				// ����Ʈ
				nt.mount();
				// ��� Ȯ��
				if (new File(nf.SDHome).exists() == false) {
					nt.runshellcommand("mkdir " + nf.SDHome);
				}
				if (new File(nf.DNHome).exists() == false) {
					nt.runshellcommand("mkdir " + nf.DNHome);
				}
				myOutput = new BufferedOutputStream(new FileOutputStream(nf.SDEdit, true));
				myOutput.write(sb.toString().getBytes());
				myOutput.close();
				nt.copyfile(nf.SDEdit, nf.DNEdit);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}
}
