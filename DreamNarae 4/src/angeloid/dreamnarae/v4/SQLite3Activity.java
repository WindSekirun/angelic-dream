package angeloid.dreamnarae.v4;

import java.io.File;
import org.holoeverywhere.preference.Preference;
import org.holoeverywhere.preference.PreferenceActivity;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import angeloid.Narae.NaraePreference;
import angeloid.Narae.NaraeTools;
import angeloid.dreamnarae.Shared.StackManager;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class SQLite3Activity extends PreferenceActivity {
	public Preference service_passive;
	public Preference service_sqlite3;
	public Preference service_last;
	public NaraePreference dn;
	public StackManager am = StackManager.getInstance();
	public NaraeTools nt = NaraeTools.getInstance(this);

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint("InlinedApi")
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle s) {
		super.onCreate(s);
		am.addActivity(this);
		addPreferencesFromResource(R.xml.sqlite3);
		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setHomeButtonEnabled(true);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowHomeEnabled(true);
		ab.setDisplayShowTitleEnabled(true);
		ab.setTitle(R.string.activity_sqlite3);
		ab.setIcon(R.drawable.ic_actionbar);
		dn = new NaraePreference(SQLite3Activity.this);
		service_passive = (Preference) findPreference("service_passive");
		service_sqlite3 = (Preference) findPreference("service_sqlite3");
		service_last = (Preference) findPreference("service_last");
		service_last.setSummary(dn.getValue("service_last_sqlite3", "null"));
		service_sqlite3.setOnPreferenceClickListener(new Click_Info());
		service_passive.setOnPreferenceClickListener(new Click_SQLite3());
		if (new File("/system/bin/narae").exists() == false) {
			AlertDialog.Builder d = new Builder(this);
			d.setMessage(getString(R.string.crouton_binary));
			d.setPositiveButton(android.R.string.cancel, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
		}
	}

	@Override
	public void onDestroy() {
		am.removeActivity(this);
		Crouton.clearCroutonsForActivity(this);
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			super.onBackPressed();;
		}
		return super.onOptionsItemSelected(item);
	}

	public class Click_SQLite3 implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference p) {
			dn.put("service_last_sqlite3", nt.getTime());
			Crouton.makeText(SQLite3Activity.this, getResources().getString(R.string.crouton_done), Style.INFO).show();
			service_last.setSummary(dn.getValue("service_last_sqlite3", "null"));
			return true;
		}
	}

	public class Click_Info implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			AlertDialog.Builder b = new AlertDialog.Builder(SQLite3Activity.this);
			WebView wv = new WebView(SQLite3Activity.this);
			if (dn.getValue("lang", "en").equals("ko")) {
				wv.loadUrl("file:///android_asset/service/sqlite3_ko.html");
			} else {
				wv.loadUrl("file:///android_asset/service/sqlite3.html");
			}
			wv.setWebViewClient(new WebViewClient() {
				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					view.loadUrl(url);
					return true;
				}
			});
			b.setView(wv);
			b.setPositiveButton(android.R.string.cancel, new OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					arg0.dismiss();
				}
			});
			b.show();
			return true;
		}
	}
}
