package angeloid.dreamnarae.v4;

import org.holoeverywhere.preference.Preference;
import org.holoeverywhere.preference.PreferenceActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import angeloid.dreamnarae.Shared.StackManager;

@SuppressLint("InlinedApi")
public class LicenseActivity extends PreferenceActivity {
	public StackManager am = StackManager.getInstance();
	public Preference apacheio, apache, crouton, dreamnarae, fstrim, gplv2, gplv3, holoeverywhere, rngd, roottools, sqlite3, zipalign, openrule, opensource, mit, aosp, xbench, design;
	public PackageInfo i;
	public String versioninfo;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		am.addActivity(this);
		addPreferencesFromResource(R.xml.license);
		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setHomeButtonEnabled(true);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowHomeEnabled(true);
		ab.setDisplayShowTitleEnabled(true);
		ab.setIcon(R.drawable.ic_actionbar);
		ab.setTitle(R.string.preference_license);
		apacheio = (Preference) findPreference("apacheio");
		apacheio.setOnPreferenceClickListener(new Click_Apacheio());
		apache = (Preference) findPreference("apache");
		apache.setOnPreferenceClickListener(new Click_Apache());
		crouton = (Preference) findPreference("crouton");
		crouton.setOnPreferenceClickListener(new Click_crouton());
		fstrim = (Preference) findPreference("fstrim");
		fstrim.setOnPreferenceClickListener(new Click_fstrim());
		gplv2 = (Preference) findPreference("gplv2");
		gplv2.setOnPreferenceClickListener(new Click_gplv2());
		gplv3 = (Preference) findPreference("gplv3");
		design = (Preference) findPreference("design");
		design.setOnPreferenceClickListener(new Click_design());
		gplv3.setOnPreferenceClickListener(new Clcik_gplv3());
		holoeverywhere = (Preference) findPreference("holoeverywhere");
		holoeverywhere.setOnPreferenceClickListener(new Click_holoeverywhere());
		rngd = (Preference) findPreference("rng-demon");
		rngd.setOnPreferenceClickListener(new Click_rngd());
		roottools = (Preference) findPreference("roottools");
		roottools.setOnPreferenceClickListener(new Click_roottools());
		sqlite3 = (Preference) findPreference("sqlite3");
		sqlite3.setOnPreferenceClickListener(new Click_sqlite3());
		zipalign = (Preference) findPreference("zipalign");
		zipalign.setOnPreferenceClickListener(new Click_zipalign());
		openrule = (Preference) findPreference("openrule");
		openrule.setOnPreferenceClickListener(new Click_openrule());
		aosp = (Preference) findPreference("aosp");
		aosp.setOnPreferenceClickListener(new Click_aosp());
		opensource = (Preference) findPreference("opensource");
		opensource.setOnPreferenceClickListener(new Click_opensource());
		mit = (Preference) findPreference("mit");
		mit.setOnPreferenceClickListener(new Click_mit());
		xbench = (Preference) findPreference("0xbench");
		xbench.setOnPreferenceClickListener(new Click_xbench());
	}

	@Override
	public void onDestroy() {
		am.removeActivity(this);
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			super.onBackPressed();;
		}
		return super.onOptionsItemSelected(item);
	}

	public void WebView(String url) {
		AlertDialog.Builder b = new AlertDialog.Builder(LicenseActivity.this);
		WebView wv = new WebView(LicenseActivity.this);
		wv.loadUrl(url);
		wv.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});
		b.setView(wv);
		b.setPositiveButton(android.R.string.cancel, new OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				arg0.dismiss();
			}
		});
		b.show();
	}

	public class Click_openrule implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/dreamnarae.html");
			return true;
		}
	}

	public class Click_xbench implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/0xbench.html");
			return true;
		}
	}

	public class Click_opensource implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/WindSekirun/DreamNarae-4"));
			startActivity(intent);
			return true;
		}
	}

	public class Click_systembartint implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/systembartint.html");
			return true;
		}
	}

	public class Click_mit implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/mit.html");
			return true;
		}
	}

	public class Click_Apacheio implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/Apache Commons IO.html");
			return true;
		}
	}

	public class Click_design implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/design.html");
			return true;
		}
	}

	public class Click_Apache implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/Apache.html");
			return true;
		}
	}

	public class Click_crouton implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/crouton.html");
			return true;
		}
	}

	public class Click_dreamnarae implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/dreamnarae.html");
			return true;
		}
	}

	public class Click_aosp implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/aosp.html");
			return true;
		}
	}

	public class Click_fstrim implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/fstrim.html");
			return true;
		}
	}

	public class Click_gplv2 implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/gplv2.html");
			return true;
		}
	}

	public class Clcik_gplv3 implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/gplv3.html");
			return true;
		}
	}

	public class Click_holoeverywhere implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/holoeverywhere.html");
			return true;
		}
	}

	public class Click_quickscroll implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/quickscroll.html");
			return true;
		}
	}

	public class Click_rngd implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/rngd.html");
			return true;
		}
	}

	public class Click_roottools implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/roottools.html");
			return true;
		}
	}

	public class Click_sqlite3 implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/sqlite3.html");
			return true;
		}
	}

	public class Click_supertooltips implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/supertooltips.html");
			return true;
		}
	}

	public class Click_zipalign implements Preference.OnPreferenceClickListener {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			WebView("file:///android_asset/license/zipalign.html");
			return true;
		}
	}
}
