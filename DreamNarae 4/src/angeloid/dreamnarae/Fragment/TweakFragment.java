package angeloid.dreamnarae.Fragment;

import java.util.ArrayList;
import java.util.Arrays;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import angeloid.Narae.NaraePreference;
import angeloid.dreamnarae.v4.AddonActivity;
import angeloid.dreamnarae.v4.CompatibleActivity;
import angeloid.dreamnarae.v4.EditActivity;
import angeloid.dreamnarae.v4.R;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class TweakFragment extends Fragment {
	public ListView list;
	public ArrayList<Menu> pd;
	public ListAdapter adapter;
	public static ArrayList<String> tweak_title;
	public static ArrayList<String> tweak_subtitle;
	public ProgressDialog mDlg;
	public NaraePreference np;
	public static Handler mc;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.menulist, container, false);
		np = new NaraePreference(getActivity());
		list = (ListView) view.findViewById(R.id.listView1);
		View header = getActivity().getLayoutInflater().inflate(R.layout.tweak, null);
		list.addHeaderView(header);
		pd = new ArrayList<Menu>();
		adapter = new ListAdapter(getActivity(), pd);
		new LoadData().execute();
		return view;
	}

	public class LoadData extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPostExecute(Void result) {
			list.setAdapter(adapter);
			list.setOnItemClickListener(new ClickMenu());
			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			tweak_title = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.tweak_menu_title)));
			tweak_subtitle = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.tweak_menu_subtitle)));
			ArrayList<Drawable> icon = new ArrayList<Drawable>();
			ArrayList<Drawable> background = new ArrayList<Drawable>();
			icon.add(getResources().getDrawable(R.drawable.list_save));
			icon.add(getResources().getDrawable(R.drawable.list_spica));
			icon.add(getResources().getDrawable(R.drawable.list_pure));
			icon.add(getResources().getDrawable(R.drawable.list_compatibity));
			icon.add(getResources().getDrawable(R.drawable.list_edit));
			icon.add(getResources().getDrawable(R.drawable.list_addon));
			background.add(getResources().getDrawable(R.drawable.aquamarine));
			background.add(getResources().getDrawable(R.drawable.royalblue));
			background.add(getResources().getDrawable(R.drawable.darkorange));
			background.add(getResources().getDrawable(R.drawable.firebrick));
			background.add(getResources().getDrawable(R.drawable.peru));
			background.add(getResources().getDrawable(R.drawable.dimgray));
			for (int i = 0; i < tweak_title.size(); i++) {
				adapter.add(new Menu(tweak_title.get(i), tweak_subtitle.get(i), background.get(i), icon.get(i), background.get(i)));
			}
			return null;
		}
	}

	public class ClickMenu implements AdapterView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			if (position == 0) {
				AlertDialog.Builder d = new AlertDialog.Builder(getActivity());
				View v = getActivity().getLayoutInflater().inflate(R.layout.runpopup, null);
				TextView tweak = (TextView) v.findViewById(R.id.tweak);
				TextView entropy = (TextView) v.findViewById(R.id.entropy);
				TextView trim = (TextView) v.findViewById(R.id.trim);
				TextView zipalign = (TextView) v.findViewById(R.id.zipalign);
				TextView sqlite3 = (TextView) v.findViewById(R.id.sqlite3);
				NaraePreference np = new NaraePreference(getActivity());
				tweak.setText(np.getValue("mode", getResources().getString(R.string.popup_notexet)));
				if (np.getValue("entropy_a", false) == true) {
					entropy.setText(getResources().getString(R.string.popup_run));
				} else {
					entropy.setText(getResources().getString(R.string.popup_notrun));
				}
				if (np.getValue("trim_a", false) == true) {
					trim.setText(getResources().getString(R.string.popup_run));
				} else {
					trim.setText(getResources().getString(R.string.popup_notrun));
				}
				if (np.getValue("sqlite3_a", false) == true) {
					sqlite3.setText(getResources().getString(R.string.popup_run));
				} else {
					sqlite3.setText(getResources().getString(R.string.popup_notrun));
				}
				if (np.getValue("zipalign_a", false) == true) {
					zipalign.setText(getResources().getString(R.string.popup_run));
				} else {
					zipalign.setText(getResources().getString(R.string.popup_notrun));
				}
				d.setView(v);
				d.setPositiveButton(android.R.string.cancel, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				d.show();
			} else if (position == 1) {
				if (np.getValue("root", false) == false) {
					Crouton.makeText(getActivity(), R.string.crouton_rootpermission, Style.ALERT).show();
				} else {}
			} else if (position == 2) {
				if (np.getValue("root", false) == false) {
					Crouton.makeText(getActivity(), R.string.crouton_rootpermission, Style.ALERT).show();
				} else {}
			} else if (position == 3) {
				if (np.getValue("root", false) == false) {
					Crouton.makeText(getActivity(), R.string.crouton_rootpermission, Style.ALERT).show();
				} else {}
			} else if (position == 4) {
				Intent i = new Intent(getActivity(), CompatibleActivity.class);
				startActivity(i);
			} else if (position == 5) {
				if (np.getValue("root", false) == false) {
					Crouton.makeText(getActivity(), R.string.crouton_rootpermission, Style.ALERT).show();
				} else {
					Intent i = new Intent(getActivity(), EditActivity.class);
					startActivity(i);
				}
			} else if (position == 6) {
				if (np.getValue("root", false) == false) {
					Crouton.makeText(getActivity(), R.string.crouton_rootpermission, Style.ALERT).show();
				} else {
					Intent i = new Intent(getActivity(), AddonActivity.class);
					startActivity(i);
				}
			}
		}
	}

	public class ListAdapter extends ArrayAdapter<Menu> {
		public LayoutInflater inflater;

		public ListAdapter(Context c, ArrayList<Menu> o) {
			super(c, 0, o);
			inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getView(int position, View v, ViewGroup parent) {
			View view = null;
			if (v == null) {
				view = inflater.inflate(R.layout.row, null);
			} else {
				view = v;
			}
			final Menu data = this.getItem(position);
			TextView title = (TextView) view.findViewById(R.id.title);
			ImageView background = (ImageView) view.findViewById(R.id.background);
			ImageView icon = (ImageView) view.findViewById(R.id.icon);
			TextView subtitle = (TextView) view.findViewById(R.id.subtitle);
			subtitle.setText(data.getSubTitle());
			title.setText(data.getTitle());
			background.setImageDrawable(data.getBackground());
			icon.setImageDrawable(data.getIcon());
			return view;
		}
	}

	public class Menu {
		String Title;
		Drawable Background;
		Drawable Icon;
		Drawable line;
		String SubTitle;

		public Menu(String t, String s, Drawable b, Drawable i, Drawable l) {
			Title = t;
			Background = b;
			Icon = i;
			SubTitle = s;
			line = l;
		}

		public String getTitle() {
			return Title;
		}

		public String getSubTitle() {
			return SubTitle;
		}

		public Drawable getBackground() {
			return Background;
		}

		public Drawable getIcon() {
			return Icon;
		}

		public Drawable getIine() {
			return line;
		}
	}

	/*public void explanationtweak(final String mode) {
		AlertDialog.Builder b = new Builder(getActivity());
		View v = getActivity().getLayoutInflater().inflate(R.layout.explan, null);
		WebView wv = (WebView) v.findViewById(R.id.webView1);
		final CheckBox cb = (CheckBox) v.findViewById(R.id.checkBox1);
		if (np.getValue("lang", "en").equals("ko")) {
			wv.loadUrl("file:///android_asset/service/trim_ko.html");
		} else {
			wv.loadUrl("file:///android_asset/service/trim.html");
		}
		b.setView(v);
		b.setPositiveButton(android.R.string.yes, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				if (cb.isChecked() == true) {
					np.put("explan", true);
				}
				if (mode.equals("P")) {
					PVersionSelect();
				} else if (mode.equals("B")) {
					BVersionSelect();
				} else if (mode.equals("S")) {
					SVersionSelect();
				}
			}
		});
		b.setNegativeButton(android.R.string.no, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		b.show();
	}*/
}