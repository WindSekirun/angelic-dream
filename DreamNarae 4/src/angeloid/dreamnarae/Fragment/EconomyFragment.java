package angeloid.dreamnarae.Fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import angeloid.Narae.NaraePreference;
import angeloid.dreamnarae.v4.BatteryActivity;
import angeloid.dreamnarae.v4.DisablerActivity;
import angeloid.dreamnarae.v4.DiskActivity;
import angeloid.dreamnarae.v4.HistoryActivity;
import angeloid.dreamnarae.v4.R;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class EconomyFragment extends Fragment {
	public ListView list;
	public ArrayList<Menu> pd;
	public ListAdapter adapter;
	public List<String> title;
	public static ArrayList<String> economy_title;
	public List<String> subtitle;
	public static ArrayList<String> economy_subtitle;
	public NaraePreference np;
	public Context c;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.menulist, container, false);
		c = getActivity();
		np = new NaraePreference(getActivity());
		list = (ListView) view.findViewById(R.id.listView1);
		View header = getActivity().getLayoutInflater().inflate(R.layout.economy, null);
		list.addHeaderView(header);
		pd = new ArrayList<Menu>();
		adapter = new ListAdapter(getActivity(), pd);
		new LoadData().execute();
		return view;
	}

	public class LoadData extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPostExecute(Void result) {
			list.setAdapter(adapter);
			list.setOnItemClickListener(new ClickMenu());
			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			title = Arrays.asList(getResources().getStringArray(R.array.economy_menu_title));
			economy_title = new ArrayList<String>(title);
			subtitle = Arrays.asList(getResources().getStringArray(R.array.economy_menu_subtitle));
			economy_subtitle = new ArrayList<String>(subtitle);
			ArrayList<Drawable> icon = new ArrayList<Drawable>();
			ArrayList<Drawable> background = new ArrayList<Drawable>();
			icon.add(getResources().getDrawable(R.drawable.list_battery));
			icon.add(getResources().getDrawable(R.drawable.list_disk));
			icon.add(getResources().getDrawable(R.drawable.list_history));
			icon.add(getResources().getDrawable(R.drawable.list_disable));
			background.add(getResources().getDrawable(R.drawable.darkorange));
			background.add(getResources().getDrawable(R.drawable.royalblue));
			background.add(getResources().getDrawable(R.drawable.mediumorchid));
			background.add(getResources().getDrawable(R.drawable.dimgray));
			for (int i = 0; i < economy_title.size(); i++) {
				adapter.add(new Menu(economy_title.get(i), economy_subtitle.get(i), background.get(i), icon.get(i), background.get(i)));
			}
			return null;
		}
	}

	public class ClickMenu implements AdapterView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			if (position == 1) {
				Intent i = new Intent(getActivity(), BatteryActivity.class);
				startActivity(i);
			} else if (position == 2) {
				Intent i = new Intent(getActivity(), DiskActivity.class);
				startActivity(i);
			} else if (position == 3) {
				Intent i = new Intent(getActivity(), HistoryActivity.class);
				startActivity(i);
			} else if (position == 4) {
				if (np.getValue("root", false) == false) {
					Crouton.makeText(getActivity(), R.string.crouton_rootpermission, Style.ALERT).show();
				} else {
					Intent i = new Intent(getActivity(), DisablerActivity.class);
					startActivity(i);
				}
			}
		}
	}

	public class ListAdapter extends ArrayAdapter<Menu> {
		private LayoutInflater inflater;

		public ListAdapter(Context c, ArrayList<Menu> o) {
			super(c, 0, o);
			inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getView(int position, View v, ViewGroup parent) {
			View view = null;
			if (v == null) {
				view = inflater.inflate(R.layout.row, null);
			} else {
				view = v;
			}
			final Menu data = this.getItem(position);
			TextView title = (TextView) view.findViewById(R.id.title);
			ImageView background = (ImageView) view.findViewById(R.id.background);
			ImageView icon = (ImageView) view.findViewById(R.id.icon);
			TextView subtitle = (TextView) view.findViewById(R.id.subtitle);
			subtitle.setText(data.getSubTitle());
			title.setText(data.getTitle());
			background.setImageDrawable(data.getBackground());
			icon.setImageDrawable(data.getIcon());
			return view;
		}
	}

	public class Menu {
		String Title;
		Drawable Background;
		Drawable Icon;
		Drawable line;
		String SubTitle;

		public Menu(String t, String s, Drawable b, Drawable i, Drawable l) {
			Title = t;
			Background = b;
			Icon = i;
			SubTitle = s;
			line = l;
		}

		public String getTitle() {
			return Title;
		}

		public String getSubTitle() {
			return SubTitle;
		}

		public Drawable getBackground() {
			return Background;
		}

		public Drawable getIcon() {
			return Icon;
		}

		public Drawable getIine() {
			return line;
		}
	}
}
