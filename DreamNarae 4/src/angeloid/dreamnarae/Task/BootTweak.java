package angeloid.dreamnarae.Task;

import java.io.File;
import android.content.Context;
import android.os.AsyncTask;
import angeloid.LilyAngels.Tweak.Addon.RunFstrim;
import angeloid.Narae.NaraeFiles;
import angeloid.Narae.NaraePreference;
import angeloid.Narae.NaraeTools;

public class BootTweak extends AsyncTask<String, String, String> {
	public Context c;
	public String mode, output;
	public String Output;
	protected BootTweak bt;
	public NaraeTools nt = NaraeTools.getInstance(c);
	public NaraeFiles nf = NaraeFiles.getInstance();

	public BootTweak(Context Context) {
		c = Context;
	}

	@Override
	protected String doInBackground(String... arg0) {
		try {
			NaraePreference dn = new NaraePreference(c);
			nt.mount();
			if (new File(nf.DNEdit).exists()) {
				nt.runshellcommand("sh " + nf.DNEdit);
			}
			if (new File(nf.DNMain).exists()) {
				nt.runshellcommand("sh " + nf.DNMain);
			}
			if (dn.getValue("entropy_a", false) == true) {
				dn.put("service_last_entropy", nt.getTime());
			}
			if (dn.getValue("trim_a", false) == true) {
				new RunFstrim(c).execute();
				dn.put("service_last_trim", nt.getTime());
			}
			if (dn.getValue("zipalign_a", false) == true) {
				dn.put("service_last_zipalign", nt.getTime());
			}
			if (dn.getValue("sqlite3_a", false) == true) {
				dn.put("service_last_sqlite3", nt.getTime());
			}
			if (dn.getValue("dns", "false").equals("true")) {}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
