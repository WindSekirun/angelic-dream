package angeloid.dreamnarae.Task;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import angeloid.Narae.NaraeFiles;
import angeloid.Narae.NaraePreference;
import angeloid.Narae.NaraeTools;
import angeloid.dreamnarae.v4.R;

/**
 * 드림나래 통합 트윅 설치 알고리즘
 * 
 * @author WindSekiurn
 * @params Tweak, Mode(Pure, Entropy... etc); *(not equals String mode)
 */
public class TweakInstall extends AsyncTask<String, String, String> {
	public static Context c;
	public String mode;
	public ProgressDialog mDlg;
	public NaraePreference np;
	public OutputStream myOutput;
	public static int bn = Build.VERSION.SDK_INT;
	public NaraeTools nt;
	public NaraeFiles nf = NaraeFiles.getInstance();

	public TweakInstall(Context Context, String Mode) {
		mode = Mode;
		c = Context;
		nt = NaraeTools.getInstance(Context);
	}

	@Override
	public String doInBackground(String... params) {
		try {
			// 나래 패키지 선언부
			np = new NaraePreference(c);
			if (mode.equals("P") || mode.equals("S") || mode.equals("B") == true) {
				// 설치 트윅 정보 입력
				np.put("mode", mode);
			} // 그 외의 경우 트윅 정보를 입력하지 않음!
				// 마운트
			nt.mount();
			// 경로 확인
			if (new File(nf.SDHome).exists() == false) {
				nt.runshellcommand("mkdir " + nf.SDHome);
			}
			if (new File(nf.DNHome).exists() == false) {
				nt.runshellcommand("mkdir " + nf.DNHome);
			}
			// 파일 출력
			myOutput = new BufferedOutputStream(new FileOutputStream(params[1], true));
			myOutput.write(params[0].getBytes());
			myOutput.flush();
			myOutput.close();
			if (mode.equals("P") || mode.equals("S") || mode.equals("B") == true) {
				myOutput = new BufferedOutputStream(new FileOutputStream(nf.SDRecovery_2, true));
				myOutput.write(params[0].getBytes());
				myOutput.flush();
				myOutput.close();
			}
			// 바이너리 파일 확인
			if (!new File(nf.SDZipAlign_f).exists() || !new File(nf.SDSQLite3_J).exists() || !new File(nf.SDSQLite3_I).exists()
					|| !new File(nf.SDRngd_J).exists() || !new File(nf.SDRngd_I).exists() || !new File(nf.SDRngd_G).exists()
					|| !new File(nf.SDFstrim).exists() || !new File(nf.SDNarae).exists()) {
				nt.copyAssets();
			}
			// 바이너리 복사
			for (int i = 0; i < nt.SDBinaryList().size(); i++) {
				nt.copyfile(nt.SDBinaryList().get(i), nt.DNBinaryList().get(i));
			}
			// 트윅 파일 복사
			nt.copyfile(nf.SDMain, nf.DNMain);
			nt.copyfile(nf.SDRecovery_2, nf.DNRecovery_2);
			// 바이너리 파일 중 안드로이드 버전 나누는 것을 선택
			if (8 <= bn) {
				if (bn <= 10) {
					nt.copyfile(nf.DNRngd_G, nf.DNRngd);
				}
				if (bn <= 15) {
					nt.copyfile(nf.DNSQLite3_I, nf.DNSQLite3_f);
				}
			}
			if (12 <= bn) {
				if (bn <= 15) {
					nt.copyfile(nf.DNRngd_I, nf.DNRngd);
				}
			}
			if (16 <= bn) {
				nt.copyfile(nf.DNRngd_J, nf.DNRngd);
				nt.copyfile(nf.DNSQLite3_J, nf.DNSQLite3_f);
			}
			// 바이너리 파일 이동(bin, xbin)
			for (int i = 0; i < nt.DNBinaryList_version().size(); i++) {
				nt.copyfile(nt.DNBinaryList_version().get(i), nt.BBinaryList().get(i));
				nt.copyfile(nt.DNBinaryList_version().get(i), nt.XBinaryList().get(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onPostExecute(String result) {
		mDlg.dismiss();
		if (mode.equals("Pure")) {
			np.put("entropy_a", true);
			np.put("trim_a", true);
			np.put("zipalign_a", true);
			np.put("sqlite3_a", true);
		} else if (mode.equals("Save")) {
			np.put("entropy_a", false);
			np.put("trim_a", false);
			np.put("zipalign_a", false);
			np.put("sqlite3_a", false);
		}
		AlertDialog.Builder naraeAlert = new AlertDialog.Builder(c);
		naraeAlert.setMessage(c.getResources().getString(R.string.alert_reboot));
		naraeAlert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				nt.rebootdevice();
			}
		});
		naraeAlert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		naraeAlert.show();
		super.onPostExecute(result);
	}

	@Override
	public void onPreExecute() {
		mDlg = new ProgressDialog(c);
		mDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mDlg.setMessage(c.getResources().getString(R.string.progress_apply));
		mDlg.setIndeterminate(false);
		mDlg.setCancelable(true);
		mDlg.setCanceledOnTouchOutside(false);
		mDlg.show();
		super.onPreExecute();
	}
}
