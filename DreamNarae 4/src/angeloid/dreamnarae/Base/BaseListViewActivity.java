package angeloid.dreamnarae.Base;

import java.util.ArrayList;
import java.util.Locale;
import org.holoeverywhere.app.Activity;
import com.stericson.RootTools.RootTools;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import angeloid.Narae.NaraePreference;
import angeloid.dreamnarae.Shared.StackManager;
import angeloid.dreamnarae.v4.Config_Build;
import angeloid.dreamnarae.v4.MainActivity;
import angeloid.dreamnarae.v4.R;

public class BaseListViewActivity extends Activity {
	public ActionBar ab;
	public StackManager am = StackManager.getInstance();
	public ActionBar.Tab Economy;
	public ActionBar.Tab Tweak;
	public ArrayList<Menu> pd;
	public ListAdapter adapter;
	public NaraePreference np;
	
	public void init() {
		Config_Build cb = Config_Build.getInstance(this);
		cb.Init();
		boolean root = RootTools.isAccessGiven();
		np.put("root", root);
		if (Locale.getDefault().getLanguage().equals("ko")) {
			np.put("lang", "ko");
		} else {
			np.put("lang", "en");
		}
		pd = new ArrayList<Menu>();
		adapter = new ListAdapter(this, pd);
		ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setHomeButtonEnabled(true);
		ab.setDisplayShowCustomEnabled(true);
		ab.setDisplayShowHomeEnabled(true);
		ab.setDisplayShowTitleEnabled(true);
		ab.setIcon(R.drawable.ic_actionbar);
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		Economy = ab.newTab().setIcon(R.drawable.ic_economy_enable);
		Tweak = ab.newTab().setIcon(R.drawable.ic_tweak_disable);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			Intent i = new Intent(this, MainActivity.class);
			startActivity(i);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		Crouton.cancelAllCroutons();
		super.onBackPressed();
	}
	
	@Override
	public void onDestroy() {
		am.removeActivity(this);
		Crouton.cancelAllCroutons();
		super.onDestroy();
	}

	public class ListAdapter extends ArrayAdapter<Menu> {
		public LayoutInflater inflater;

		public ListAdapter(Context c, ArrayList<Menu> o) {
			super(c, 0, o);
			inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getView(int position, View v, ViewGroup parent) {
			View view = null;
			if (v == null) {
				view = inflater.inflate(R.layout.row, null);
			} else {
				view = v;
			}
			final Menu data = this.getItem(position);
			TextView title = (TextView) view.findViewById(R.id.title);
			ImageView background = (ImageView) view.findViewById(R.id.background);
			ImageView icon = (ImageView) view.findViewById(R.id.icon);
			TextView subtitle = (TextView) view.findViewById(R.id.subtitle);
			subtitle.setText(data.getSubTitle());
			title.setText(data.getTitle());
			background.setImageDrawable(data.getBackground());
			icon.setImageDrawable(data.getIcon());
			return view;
		}
	}

	public class Menu {
		String Title, SubTitle;
		Drawable Background, Icon;

		public Menu(String t, String s, Drawable b, Drawable i) {
			Title = t;
			Background = b;
			Icon = i;
			SubTitle = s;
		}

		public String getTitle() {
			return Title;
		}

		public String getSubTitle() {
			return SubTitle;
		}

		public Drawable getBackground() {
			return Background;
		}

		public Drawable getIcon() {
			return Icon;
		}
	}
}
