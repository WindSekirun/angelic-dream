package angeloid.dreamnarae.Shared;

public class StopWatchAverage {
	public long startTime;
	public long elapsedTime = 0;
	public double totalElapsedTime;
	public long runCount = 0;
	public String currentName;
	public boolean threadFlag = false;

	public StopWatchAverage() {
		currentName = "";
		startTime = System.nanoTime();
	}

	public StopWatchAverage(boolean threadFlag) {
		changeMessage("", true, true);
	}

	public StopWatchAverage(String message) {
		changeMessage(message, false, true);
	}

	public StopWatchAverage(String message, boolean threadFlag) {
		changeMessage(message, threadFlag, true);
	}

	public void reset() {
		startTime = System.nanoTime();
		elapsedTime = 0;
		totalElapsedTime = 0;
		runCount = 0;
	}

	public void start() {
		startTime = System.nanoTime();
		elapsedTime = 0;
	}

	public void stop() {
		elapsedTime = System.nanoTime() - startTime;
		totalElapsedTime += elapsedTime;
		runCount++;
	}

	public void changeMessage(String message, boolean threadFlag, boolean resetFlag) {
		String threadName = "";
		this.threadFlag = threadFlag;
		if (threadFlag) {
			threadName = " ThreadName = " + Thread.currentThread().getName();
		}
		currentName = "[" + message + threadName + "] ";
		if (resetFlag) {
			reset();
		}
	}

	public double getElapsedMS() {
		if (elapsedTime == 0) stop();
		return elapsedTime / 1000000.0;
	}

	public double getElapsedNano() {
		if (elapsedTime == 0) {
			stop();
		}
		return elapsedTime;
	}

	public String toTotalTime() {
		return String.valueOf(totalElapsedTime / 1000000.0);
	}

	public String toString() {
		if (elapsedTime == 0) {
			stop();
		}
		double elapsedAverage = totalElapsedTime / runCount;
		return currentName + "Run Count : " + runCount + " , Total : " + totalElapsedTime / 1000000.0 + " ms, Average : " + elapsedAverage
				/ 1000000.0 + " ms";
	}
}