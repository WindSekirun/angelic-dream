package angeloid.dreamnarae.Shared;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import angeloid.Narae.NaraePreference;
import angeloid.dreamnarae.Task.BootTweak;
import angeloid.dreamnarae.v4.MainActivity;
import angeloid.dreamnarae.v4.R;

public class BootReceiver extends BroadcastReceiver {
	public NotificationManager manager;
	public NotificationCompat.Builder ncbuilder;

	@Override
	public void onReceive(Context c, Intent i) {
		NaraePreference np = new NaraePreference(c);
		manager = (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
		ncbuilder = new NotificationCompat.Builder(c);
		ncbuilder.setContentTitle(c.getResources().getString(R.string.app_name));
		ncbuilder.setAutoCancel(true);
		ncbuilder.setTicker(c.getResources().getString(R.string.app_name));
		i = new Intent(c, MainActivity.class);
		PendingIntent pi = PendingIntent.getActivity(c, 0, i, 0);
	    ncbuilder.setContentIntent(pi);
		if (np.getValue("root", false) == true) {
			new BootTweak(c).execute();
			ncbuilder.setContentText(c.getResources().getString(R.string.crouton_done));
			ncbuilder.setSmallIcon(R.drawable.noti_success);
			if (np.getValue("bootnoti", true) == true) {
				manager.notify(0, ncbuilder.build());
			}
		} else {
			ncbuilder.setContentText(c.getResources().getString(R.string.crouton_rootpermission));
			ncbuilder.setSmallIcon(R.drawable.noti_fail);
			if (np.getValue("bootnoti", true) == true) {
				manager.notify(0, ncbuilder.build());
			}
		}
	}
}
