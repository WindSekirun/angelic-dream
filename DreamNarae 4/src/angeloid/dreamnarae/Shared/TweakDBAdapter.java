package angeloid.dreamnarae.Shared;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class TweakDBAdapter {
	public static final String KEY_FILE = "filename";
	public static final String KEY_TIME = "time";
	public static final String KEY_ROWID = "_id";
	public static final String TAG = "TweakDBAdapter";
	public DatabaseHelper mDbHelper;
	public SQLiteDatabase mDb;
	public static final String DATABASE_CREATE = "create table tweak (_id integer primary key autoincrement, "
			+ "filename text not null, time text not null);";
	public static final String DATABASE_NAME = "data";
	public static final String DATABASE_TABLE = "tweak";
	public static final int DATABASE_VERSION = 2;
	public Context mCtx;

	public static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS notes");
			onCreate(db);
		}
	}

	public TweakDBAdapter(Context ctx) {
		this.mCtx = ctx;
	}

	public TweakDBAdapter open() throws SQLException {
		mDbHelper = new DatabaseHelper(mCtx);
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		mDbHelper.close();
	}

	public long createFileLog(String title, String body) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_FILE, title);
		initialValues.put(KEY_TIME, body);
		return mDb.insert(DATABASE_TABLE, null, initialValues);
	}

	public boolean deleteFileLog(long rowId) {
		return mDb.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
	}

	public Cursor fetchAllFileLog() {
		return mDb.query(DATABASE_TABLE, new String[] { KEY_ROWID, KEY_FILE, KEY_TIME }, null, null, null, null, null);
	}

	public Cursor fetchFileLog(long rowId) throws SQLException {
		Cursor mCursor = mDb.query(true, DATABASE_TABLE, new String[] { KEY_ROWID, KEY_FILE, KEY_TIME }, KEY_ROWID + "=" + rowId, null,
				null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	public boolean updateFileLog(long rowId, String filename, String time) {
		ContentValues args = new ContentValues();
		args.put(KEY_FILE, filename);
		args.put(KEY_TIME, time);
		return mDb.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
	}
}
