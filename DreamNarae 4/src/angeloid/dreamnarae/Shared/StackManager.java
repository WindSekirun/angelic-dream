package angeloid.dreamnarae.Shared;

import java.util.ArrayList;
import android.app.Activity;

public class StackManager {
	public static StackManager activityMananger = null;
	public ArrayList<Activity> activityList = null;

	public StackManager() {
		activityList = new ArrayList<Activity>();
	}

	public static StackManager getInstance() {
		if (StackManager.activityMananger == null) {
			activityMananger = new StackManager();
		}
		return activityMananger;
	}

	public void addActivity(Activity activity) {
		activityList.add(activity);
	}

	public boolean removeActivity(Activity activity) {
		return activityList.remove(activity);
	}

	public void finishAllActivity() {
		for (Activity activity : activityList) {
			activity.finish();
		}
	}
}
