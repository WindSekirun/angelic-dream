package angeloid.Narae;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import android.content.Context;
import android.content.res.AssetManager;
import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.execution.CommandCapture;

// Instance Method
public class NaraeTools {
	public Context c;
	public String[] files;
	public InputStream in;
	public OutputStream out;
	protected static NaraeTools nt;
    public NaraeFiles nf = NaraeFiles.getInstance();

	private NaraeTools(Context c) {
		this.c = c;
	}

	public static NaraeTools getInstance(Context c) {
		if (nt == null) {
			nt = new NaraeTools(c);
		}
		return nt;
	}

	public String getTime() {
		return java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
	}

	public void mount() {
		RootTools.remount("/system", "rw");
		RootTools.remount("/data", "rw");
		runshellcommand("mount -o rw,remount /system");
		runshellcommand("mount -o rw,remount /data");
	}

	public void chmod() {
		runshellcommand("chmod 777 " + nf.DNHome);
		runshellcommand("chmod 777 " + nf.DNinit);
		for (int i = 0; i < DNBinaryList().size(); i++) {
			runshellcommand("chmod 777 " + DNBinaryList().get(i));
		}
	}

	public void runshellcommand(String... shell) {
		for (int i = 0; i < shell.length; i++) {
			CommandCapture c = new CommandCapture(0, shell[i]);
			try {
				RootTools.getShell(true).add(c);
			} catch (Exception e) {}
		}
	}

	public void copyfile(String from, String dest) {
		CommandCapture command = new CommandCapture(0, "cat " + from + " > " + dest);
		try {
			RootTools.getShell(true).add(command);
		} catch (Exception e) {}
	}

	public void deletehistorytweak() {
		runshellcommand("rm /system/etc/init.d/00Brand");
		runshellcommand("rm /system/etc/init.d/00Miracle");
		runshellcommand("rm /system/etc/init.d/00Pure");
		runshellcommand("rm /system/etc/init.d/00Prev");
		runshellcommand("rm /system/etc/init.d/00Save");
		runshellcommand("rm /system/etc/init.d/00SPiCa");
		runshellcommand("rm /system/etc/init.d/00SPiSave");
	}

	public void copyAssets() {
		try {
			AssetManager assetManager = c.getAssets();
			files = assetManager.list("Files");
			for (String filename : files) {
				in = assetManager.open("Files/" + filename);
				out = new FileOutputStream(nf.SDHome + "/" + filename);
				copyfiles(in, out);
				in.close();
				in = null;
				out.flush();
				out.close();
				out = null;
			}
		} catch (Exception e) {}
	}

	public void copyfiles(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		int read;
		while ((read = in.read(buffer)) != -1) {
			out.write(buffer, 0, read);
		}
	}

	public void rebootdevice() {
		CommandCapture command = new CommandCapture(0, "reboot");
		try {
			RootTools.getShell(true).add(command);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<String> SDBinaryList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add(nf.SDNarae);
		list.add(nf.SDRngd_G);
		list.add(nf.SDRngd_I);
		list.add(nf.SDRngd_J);
		list.add(nf.SDSQLite3_I);
		list.add(nf.SDSQLite3_J);
		list.add(nf.SDFstrim);
		list.add(nf.SDZipAlign_f);
		return list;
	}

	public ArrayList<String> DNBinaryList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add(nf.DNNarae);
		list.add(nf.DNRngd_G);
		list.add(nf.DNRngd_I);
		list.add(nf.DNRngd_J);
		list.add(nf.DNSQLite3_I);
		list.add(nf.DNSQLite3_J);
		list.add(nf.DNFstrim);
		list.add(nf.DNZipAlign_f);
		return list;
	}

	public ArrayList<String> DNBinaryList_version() {
		ArrayList<String> list = new ArrayList<String>();
		list.add(nf.DNNarae);
		list.add(nf.DNRngd);
		list.add(nf.DNSQLite3_f);
		list.add(nf.DNFstrim);
		list.add(nf.DNZipAlign_f);
		return list;
	}

	public ArrayList<String> BBinaryList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add(nf.BNarae);
		list.add(nf.BRngd);
		list.add(nf.BSQLite3_f);
		list.add(nf.BFstrim);
		list.add(nf.BZipAlign_f);
		return list;
	}

	public ArrayList<String> XBinaryList() {
		ArrayList<String> list = new ArrayList<String>();
		list.add(nf.XNarae);
		list.add(nf.XRngd);
		list.add(nf.XSQLite3_f);
		list.add(nf.XFstrim);
		list.add(nf.XZipAlign_f);
		return list;
	}
}
