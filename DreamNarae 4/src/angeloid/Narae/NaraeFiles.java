package angeloid.Narae;

import android.os.Environment;

// Instance Method
public class NaraeFiles {
	protected static NaraeFiles nf;

	private NaraeFiles() {
	}

	public static NaraeFiles getInstance() {
		if (nf == null) {
			nf = new NaraeFiles();
		}
		return nf;
	}

	public String SDCard = Environment.getExternalStorageDirectory().toString();
	public String SDHome = SDCard + "/dreamnarae";
	public String SDFstrim = SDHome + "/fstrim";
	public String SDNarae = SDHome + "/narae";
	public String SDRngd_G = SDHome + "/rngd_G";
	public String SDRngd_I = SDHome + "/rngd_I";
	public String SDRngd_J = SDHome + "/rngd_j";
	public String SDSQLite3_I = SDHome + "/sqlite3_i";
	public String SDSQLite3_J = SDHome + "/sqlite3_j";
	public String SDZipAlign_f = SDHome + "/zipalign";
	public String SDEntropy = SDHome + "/entropy.sh";
	public String SDSQLite3 = SDHome + "/sqlite3.sh";
	public String SDZipAlign = SDHome + "/zipalign.sh";
	public String SDMain = SDHome + "/main.sh";
	public String SDEdit = SDHome + "/edit.sh";
	public String SDRecovery_1 = SDHome + "/install-recovery.sh";
	public String SDRecovery_2 = SDHome + "/install-recovery-2.sh";
	public String System = "/system";
	public String DNHome = System + "/etc/dreamnarae";
	public String DNFstrim = System + "/fstrim";
	public String DNNarae = System + "/narae";
	public String DNRngd_G = System + "/rngd_G";
	public String DNRngd_I = System + "/rngd_I";
	public String DNRngd_J = System + "/rngd_j";
	public String DNRngd = System + "/rngd";
	public String DNSQLite3_f = System + "/sqlite3";
	public String DNSQLite3_I = System + "/sqlite3_i";
	public String DNSQLite3_J = System + "/sqlite3_j";
	public String DNZipAlign_f = System + "/zipalign";
	public String DNEntropy = System + "/entropy.sh";
	public String DNSQLite3 = System + "/sqlite3.sh";
	public String DNZipAlign = System + "/zipalign.sh";
	public String DNMain = System + "/main.sh";
	public String DNEdit = System + "/edit.sh";
	public String DNinit = System + "/etc/init.d";
	public String DNRecovery_1 = System + "/etc/install-recovery.sh";
	public String DNRecovery_2 = System + "/etc/install-recovery-2.sh";
	public String Bin = "/system/bin";
	public String BFstrim = Bin + "/fstrim";
	public String BNarae = Bin + "/narae";
	public String BRngd = Bin + "/rngd";
	public String BZipAlign_f = Bin + "/zipalign";
	public String BSQLite3_f = Bin + "/sqlite3";
	public String Xbin = "/system/xbin";
	public String XFstrim = Xbin + "/fstrim";
	public String XNarae = Xbin + "/narae";
	public String XRngd = Xbin + "/rngd";
	public String XZipAlign_f = Xbin + "/zipalign";
	public String XSQLite3_f = Xbin + "/sqlite3";
	public String Data = "/data";
}
