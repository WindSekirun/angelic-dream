package angeloid.LilyAngels.Tweak.Balance;

import java.util.ArrayList;
import java.util.Scanner;

public class SPiCa {
	public static ArrayList<String> data;
	public static Scanner s;

	public String getTweak(boolean isNarae) {
		String string = new String();
		if (isNarae = true) {
			string = Narae();
		} else {
			string = Setprop();
		}
		return string;
	}

	public static String Setprop() {
		StringBuilder sb = new StringBuilder();
		sb.append(SPiCa_Composition.getTweak(false));
		sb.append(SPiCa_CpuLevel.getTweak());
		sb.append(SPiCa_Execution.getTweak(false));
		sb.append(SPiCa_RM.getTweak());
		sb.append(SPiCa_SDCard.getTweak());
		sb.append(SPiCa_Setprop.getTweak(false));
		sb.append(SPiCa_Sleep.getTweak(false));
		sb.append(SPiCa_Swap.getTweak(60));
		sb.append(SPiCa_VM.getTweak());
		return sb.toString();
	}

	public static String Narae() {
		StringBuilder sb = new StringBuilder();
		sb.append(SPiCa_Composition.getTweak(true));
		sb.append(SPiCa_CpuLevel.getTweak());
		sb.append(SPiCa_Execution.getTweak(true));
		sb.append(SPiCa_RM.getTweak());
		sb.append(SPiCa_SDCard.getTweak());
		sb.append(SPiCa_Setprop.getTweak(true));
		sb.append(SPiCa_Sleep.getTweak(true));
		sb.append(SPiCa_Swap.getTweak(60));
		sb.append(SPiCa_VM.getTweak());
		return sb.toString();
	}

	public static Double getSize() {
		s = new Scanner(Setprop());
		while (s.hasNext()) {
			data.add(Setprop());
		}
		Double result = (double) data.size();
		return result;
	}
}
