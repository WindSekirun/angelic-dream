package angeloid.LilyAngels.Tweak.Balance;

import android.os.Build;

public class SPiCa_Sleep {
	public static String getTweak(boolean isNarae) {
		String string = new String();
		if (isNarae = true) {
			string = Narae();
		} else {
			string = Setprop();
		}
		return string;
	}

	public static String Setprop() {
		StringBuilder sb = new StringBuilder();
		int version = Build.VERSION.SDK_INT;
		if (version == Build.VERSION_CODES.FROYO) {
			sb.append("setprop pm.sleep_mode 1");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.GINGERBREAD) {
			sb.append("setprop pm.sleep_mode 2");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.GINGERBREAD_MR1) {
			sb.append("setprop pm.sleep_mode 2");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.HONEYCOMB) {} else if (version == Build.VERSION_CODES.HONEYCOMB_MR1) {} else if (version == Build.VERSION_CODES.HONEYCOMB_MR2) {} else if (version == Build.VERSION_CODES.ICE_CREAM_SANDWICH) {} else if (version == Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {} else if (version == Build.VERSION_CODES.JELLY_BEAN) {} else if (version == Build.VERSION_CODES.JELLY_BEAN_MR1) {} else if (version == Build.VERSION_CODES.JELLY_BEAN_MR2) {} else if (version == Build.VERSION_CODES.KITKAT) {}
		String string = sb.toString();
		return string;
	}

	public static String Narae() {
		StringBuilder sb = new StringBuilder();
		int version = Build.VERSION.SDK_INT;
		if (version == Build.VERSION_CODES.FROYO) {
			sb.append("narae pm.sleep_mode 1 ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.GINGERBREAD) {
			sb.append("narae pm.sleep_mode 2 ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.GINGERBREAD_MR1) {
			sb.append("narae pm.sleep_mode 2 ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.HONEYCOMB) {} else if (version == Build.VERSION_CODES.HONEYCOMB_MR1) {} else if (version == Build.VERSION_CODES.HONEYCOMB_MR2) {} else if (version == Build.VERSION_CODES.ICE_CREAM_SANDWICH) {} else if (version == Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {} else if (version == Build.VERSION_CODES.JELLY_BEAN) {} else if (version == Build.VERSION_CODES.JELLY_BEAN_MR1) {} else if (version == Build.VERSION_CODES.JELLY_BEAN_MR2) {} else if (version == Build.VERSION_CODES.KITKAT) {}
		String string = sb.toString();
		return string;
	}
}
