package angeloid.LilyAngels.Tweak.Performance;

import android.os.Build;

public class Pure_Composition {
	public static String getTweak(boolean isNarae) {
		String string = new String();
		if (isNarae = true) {
			string = Narae();
		} else {
			string = Setprop();
		}
		return string;
	}

	public static String getSTE(boolean isNarae, String type) {
		StringBuilder sb = new StringBuilder();
		if (isNarae = true) {
			if (type == "dyn") {
				sb.append("narae debug.composition.type dyn ");
				sb.append(System.getProperty("line.separator"));
			} else if (type == "cpu") {
				sb.append("narae debug.composition.type cpu ");
				sb.append(System.getProperty("line.separator "));
			} else if (type == "gpu") {
				sb.append("narae debug.composition.type gpu ");
				sb.append(System.getProperty("line.separator"));
			} else if (type == "m2d") {
				sb.append("narae debug.composition.type m2d ");
				sb.append(System.getProperty("line.separator"));
			}
		} else {
			if (type == "dyn") {
				sb.append("setprop debug.composition.type dyn");
				sb.append(System.getProperty("line.separator"));
			} else if (type == "cpu") {
				sb.append("setprop debug.composition.type cpu");
				sb.append(System.getProperty("line.separator"));
			} else if (type == "gpu") {
				sb.append("setprop debug.composition.type gpu");
				sb.append(System.getProperty("line.separator"));
			} else if (type == "m2d") {
				sb.append("setprop debug.composition.type m2d");
				sb.append(System.getProperty("line.separator"));
			}
		}
		return sb.toString();
	}

	public static String Setprop() {
		StringBuilder sb = new StringBuilder();
		int version = Build.VERSION.SDK_INT;
		if (version == Build.VERSION_CODES.FROYO) {} else if (version == Build.VERSION_CODES.GINGERBREAD) {} else if (version == Build.VERSION_CODES.GINGERBREAD_MR1) {} else if (version == Build.VERSION_CODES.HONEYCOMB) {
			sb.append("setprop debug.composition.type cpu");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.HONEYCOMB_MR1) {
			sb.append("setprop debug.composition.type cpu");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.HONEYCOMB_MR2) {
			sb.append("setprop debug.composition.type cpu");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			sb.append("setprop debug.composition.type cpu");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
			sb.append("setprop debug.composition.type cpu");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.JELLY_BEAN) {
			sb.append("setprop debug.composition.type dyn");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.JELLY_BEAN_MR1) {
			sb.append("setprop debug.composition.type dyn");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.JELLY_BEAN_MR2) {
			sb.append("setprop debug.composition.type dyn");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.KITKAT) {
			sb.append("setprop debug.composition.type dyn");
			sb.append(System.getProperty("line.separator"));
		}
		String string = sb.toString();
		return string;
	}

	public static String Narae() {
		StringBuilder sb = new StringBuilder();
		int version = Build.VERSION.SDK_INT;
		if (version == Build.VERSION_CODES.FROYO) {} else if (version == Build.VERSION_CODES.GINGERBREAD) {} else if (version == Build.VERSION_CODES.GINGERBREAD_MR1) {} else if (version == Build.VERSION_CODES.HONEYCOMB) {
			sb.append("narae debug.composition.type cpu ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.HONEYCOMB_MR1) {
			sb.append("narae debug.composition.type cpu ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.HONEYCOMB_MR2) {
			sb.append("narae debug.composition.type cpu ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			sb.append("narae debug.composition.type cpu ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
			sb.append("narae debug.composition.type cpu ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.JELLY_BEAN) {
			sb.append("narae debug.composition.type dyn ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.JELLY_BEAN_MR1) {
			sb.append("narae debug.composition.type dyn ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.JELLY_BEAN_MR2) {
			sb.append("narae debug.composition.type dyn ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.KITKAT) {
			sb.append("narae debug.composition.type dyn ");
			sb.append(System.getProperty("line.separator"));
		}
		String string = sb.toString();
		return string;
	}
}
