package angeloid.LilyAngels.Tweak.Performance;

import android.os.Build;

public class Pure_Execution {
	public static String getTweak(boolean isNarae) {
		String string = new String();
		if (isNarae = true) {
			string = Narae();
		} else {
			string = Setprop();
		}
		return string;
	}

	public static String Setprop() {
		StringBuilder sb = new StringBuilder();
		int version = Build.VERSION.SDK_INT;
		if (version == Build.VERSION_CODES.FROYO) {
			sb.append("setprop dalvik.vm.execution-mode int:jit");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.GINGERBREAD) {
			sb.append("setprop dalvik.vm.execution-mode int:jit");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.GINGERBREAD_MR1) {
			sb.append("setprop dalvik.vm.execution-mode int:jit");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.HONEYCOMB) {
			sb.append("setprop dalvik.vm.execution-mode int:fast");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.HONEYCOMB_MR1) {
			sb.append("setprop dalvik.vm.execution-mode int:fast");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.HONEYCOMB_MR2) {
			sb.append("setprop dalvik.vm.execution-mode int:fast");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			sb.append("setprop dalvik.vm.execution-mode int:fast");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
			sb.append("setprop dalvik.vm.execution-mode int:fast");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.JELLY_BEAN) {
			sb.append("setprop dalvik.vm.execution-mode int:fast");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.JELLY_BEAN_MR1) {
			sb.append("setprop dalvik.vm.execution-mode int:fast");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.JELLY_BEAN_MR2) {
			sb.append("setprop dalvik.vm.execution-mode int:fast");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.KITKAT) {
			sb.append("setprop dalvik.vm.execution-mode int:fast");
			sb.append(System.getProperty("line.separator"));
		}
		String string = sb.toString();
		return string;
	}

	public static String Narae() {
		StringBuilder sb = new StringBuilder();
		int version = Build.VERSION.SDK_INT;
		if (version == Build.VERSION_CODES.FROYO) {
			sb.append("narae dalvik.vm.execution-mode int:jit ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.GINGERBREAD) {
			sb.append("narae dalvik.vm.execution-mode int:jit ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.GINGERBREAD_MR1) {
			sb.append("narae dalvik.vm.execution-mode int:jit ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.HONEYCOMB) {
			sb.append("narae dalvik.vm.execution-mode int:fast ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.HONEYCOMB_MR1) {
			sb.append("narae dalvik.vm.execution-mode int:fast ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.HONEYCOMB_MR2) {
			sb.append("narae dalvik.vm.execution-mode int:fast ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			sb.append("narae dalvik.vm.execution-mode int:fast ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
			sb.append("narae dalvik.vm.execution-mode int:fast ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.JELLY_BEAN) {
			sb.append("narae dalvik.vm.execution-mode int:fast ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.JELLY_BEAN_MR1) {
			sb.append("narae dalvik.vm.execution-mode int:fast ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.JELLY_BEAN_MR2) {
			sb.append("narae dalvik.vm.execution-mode int:fast ");
			sb.append(System.getProperty("line.separator"));
		} else if (version == Build.VERSION_CODES.KITKAT) {
			sb.append("narae dalvik.vm.execution-mode int:fast ");
			sb.append(System.getProperty("line.separator"));
		}
		String string = sb.toString();
		return string;
	}
}
