package angeloid.LilyAngels.Tweak.Battery;

public class Save_Swap {
	public static String getTweak(int Swap) {
		if (Swap > 100) {
			Swap = 60;
		}
		if (Swap < 0) {
			Swap = 60;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("if [ -e /proc/sys/vm/swappiness ]; then");
		sb.append(System.getProperty("line.separator"));
		sb.append("	echo " + Swap + " > /proc/sys/vm/swappiness");
		sb.append(System.getProperty("line.separator"));
		sb.append("fi");
		sb.append(System.getProperty("line.separator"));
		return sb.toString();
	}
}
