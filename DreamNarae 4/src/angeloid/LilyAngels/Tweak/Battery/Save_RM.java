package angeloid.LilyAngels.Tweak.Battery;

public class Save_RM {
	public static String getTweak() {
		StringBuilder sb = new StringBuilder();
		sb.append("rm -r /data/local/tmp/*");
		sb.append(System.getProperty("line.separator"));
		sb.append("rm -r /data/tmp/*");
		sb.append(System.getProperty("line.separator"));
		sb.append("rm -r /data/system/usagestats/*");
		sb.append(System.getProperty("line.separator"));
		sb.append("rm -r /data/system/appusagestats/*");
		sb.append(System.getProperty("line.separator"));
		sb.append("rm -r /data/system/dropbox/*");
		sb.append(System.getProperty("line.separator"));
		return sb.toString();
	}
}
