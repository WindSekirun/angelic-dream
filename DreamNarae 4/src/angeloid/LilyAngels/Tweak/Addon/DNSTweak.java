package angeloid.LilyAngels.Tweak.Addon;

import java.util.ArrayList;
import java.util.Scanner;

public class DNSTweak {
	static StringBuilder sb;
	public static ArrayList<String> data;
	public static Scanner s;

	public static String getTweak(boolean isNarae) {
		if (isNarae == true) {
			return Narae();
		} else {
			return Setprop();
		}
	}

	public static String Setprop() {
		sb.append("setprop net.change net.dnschange");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop net.dns2 8.8.4.4");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop net.rmnet0.dns2 8.8.4.4");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop net.wlan0.dns2 8.8.4.4");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop dhcp.wlan0.dns2 8.8.4.4");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop dhcp.eth0.dns2 8.8.4.4");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop net.wlan0.dns3 8.8.8.8");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop dhcp.wlan0.dns3 8.8.8.8");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop dhcp.eth0.dns3 8.8.8.8");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop net.dns1 127.0.0.1");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop net.rmnet0.dns1 127.0.0.1");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop net.wlan0.dns1 127.0.0.1");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop dhcp.wlan0.dns1 127.0.0.1");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop dhcp.eth0.dns1 127.0.0.1");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop net.wlan0.dns4 208.67.222.222");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop dhcp.wlan0.dns4 208.67.222.222");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop dhcp.eth0.dns4 208.67.222.222");
		sb.append(System.getProperty("line.separator"));
		return sb.toString();
	}

	public static String Narae() {
		sb.append("narae net.change net.dnschange ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae net.dns2 8.8.4.4 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae net.rmnet0.dns2 8.8.4.4 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae net.wlan0.dns2 8.8.4.4 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae dhcp.wlan0.dns2 8.8.4.4 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae dhcp.eth0.dns2 8.8.4.4 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae net.wlan0.dns3 8.8.8.8 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae dhcp.wlan0.dns3 8.8.8.8 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae dhcp.eth0.dns3 8.8.8.8 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae net.dns1 127.0.0.1 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae net.rmnet0.dns1 127.0.0.1 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae net.wlan0.dns1 127.0.0.1 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae dhcp.wlan0.dns1 127.0.0.1 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae dhcp.eth0.dns1 127.0.0.1 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae net.wlan0.dns4 208.67.222.222 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae dhcp.wlan0.dns4 208.67.222.222 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae dhcp.eth0.dns4 208.67.222.222 ");
		sb.append(System.getProperty("line.separator"));
		return sb.toString();
	}

	public static Double getSize() {
		s = new Scanner(Setprop());
		while (s.hasNext()) {
			data.add(Setprop());
		}
		Double result = (double) data.size();
		return result;
	}
}
