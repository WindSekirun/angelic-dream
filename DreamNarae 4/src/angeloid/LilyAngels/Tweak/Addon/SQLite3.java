package angeloid.LilyAngels.Tweak.Addon;

public class SQLite3 {
	public static String getTweak() {
		StringBuilder sb = new StringBuilder();
		sb.append("for i in `busybox find /d* -iname \"*.db\"`; do");
		sb.append(System.getProperty("line.separator"));
		sb.append("/system/xbin/sqlite3 $i 'VACUUM;';");
		sb.append(System.getProperty("line.separator"));
		sb.append("resVac=$?");
		sb.append(System.getProperty("line.separator"));
		sb.append("if [ $resVac == 0 ]; then");
		sb.append(System.getProperty("line.separator"));
		sb.append("resVac=\"SUCCESS\";");
		sb.append(System.getProperty("line.separator"));
		sb.append("else");
		sb.append(System.getProperty("line.separator"));
		sb.append("resVac=\"ERRCODE-$resVac\";");
		sb.append(System.getProperty("line.separator"));
		sb.append("fi;");
		sb.append(System.getProperty("line.separator"));
		sb.append("/system/xbin/sqlite3 $i 'REINDEX;';");
		sb.append(System.getProperty("line.separator"));
		sb.append("resIndex=$?");
		sb.append(System.getProperty("line.separator"));
		sb.append("if [ $resIndex == 0 ]; then");
		sb.append(System.getProperty("line.separator"));
		sb.append("resIndex=\"SUCCESS\";");
		sb.append(System.getProperty("line.separator"));
		sb.append("else");
		sb.append(System.getProperty("line.separator"));
		sb.append("resIndex=\"ERRCODE-$resIndex\";");
		sb.append(System.getProperty("line.separator"));
		sb.append("fi;");
		sb.append(System.getProperty("line.separator"));
		sb.append("done");
		sb.append(System.getProperty("line.separator"));
		return sb.toString();
	}
}
