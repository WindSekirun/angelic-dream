package angeloid.LilyAngels.Tweak.Addon;

public class Entropy {
	static StringBuilder sb;

	public static String getTweak() {
		sb = new StringBuilder();
		sb.append("rngd=/system/xbin/rngd");
		sb.append(System.getProperty("line.separator"));
		sb.append("rngd_backup=/system/bin/rngd.bak");
		sb.append(System.getProperty("line.separator"));
		sb.append("busybox rm -f \"$haveged_log\"");
		sb.append(System.getProperty("line.separator"));
		sb.append("busybox mv -f $rngd_backup $rngd");
		sb.append(System.getProperty("line.separator"));
		sb.append("busybox chmod -f 755 \"$rngd\"");
		sb.append(System.getProperty("line.separator"));
		sb.append("busybox chown -f 0.0 \"$rngd\"");
		sb.append(System.getProperty("line.separator"));
		sb.append("rngd -t 2 -T 1 -s 256 --fill-watermark=80%");
		sb.append(System.getProperty("line.separator"));
		sb.append("busybox renice 5 `pidof rngd`");
		sb.append(System.getProperty("line.separator"));
		return sb.toString();
	}
}
