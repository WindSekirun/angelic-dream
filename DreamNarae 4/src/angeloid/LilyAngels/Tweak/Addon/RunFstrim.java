package angeloid.LilyAngels.Tweak.Addon;

import android.content.Context;
import android.os.AsyncTask;
import angeloid.Narae.NaraeTools;

public class RunFstrim extends AsyncTask<String, String, String> {
	public Context c;

	public RunFstrim(Context c) {
		this.c = c;
	}

	@Override
	protected String doInBackground(String... arg0) {
		NaraeTools nt = NaraeTools.getInstance(c);
		nt.runshellcommand("fstrim /system");
		nt.runshellcommand("fstrim /data");
		nt.runshellcommand("fstrim /cache");
		return null;
	}
}
